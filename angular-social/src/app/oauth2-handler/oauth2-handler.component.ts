import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-oauth2-handler',
  templateUrl: './oauth2-handler.component.html',
  styleUrls: ['./oauth2-handler.component.css']
})
export class OAuth2HandlerComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
    var token = window.location.search.slice(1).split('&')[0].split('=')[1];

    if(token) {
        localStorage.setItem("accessToken", token); 
        this.router.navigate(['/']);
    } else {
       
    }
  }

}
