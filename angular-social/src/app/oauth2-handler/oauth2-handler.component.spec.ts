import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OAuth2HandlerComponent } from './oauth2-handler.component';

describe('OAuth2HandlerComponent', () => {
  let component: OAuth2HandlerComponent;
  let fixture: ComponentFixture<OAuth2HandlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OAuth2HandlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OAuth2HandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
