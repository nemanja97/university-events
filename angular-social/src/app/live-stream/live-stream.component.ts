import { Component, OnInit } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { LiveStreamService } from '../services/Live-Stream/live-stream.service';
import { ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import Faculty from 'src/app/models/faculty';
import Event from 'src/app/models/event';
import University from 'src/app/models/university';
import Place from 'src/app/models/place';
import { EventService } from 'src/app/services/Event/event.service';
import * as moment from 'moment';
import User from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/Login/Authentication/authentication.service';
import { UserService } from '../services/User/user.service';

@Component({
  selector: 'app-live-stream',
  templateUrl: './live-stream.component.html',
  styleUrls: ['./live-stream.component.css']
})
export class LiveStreamComponent implements OnInit {

  constructor(private liveService: LiveStreamService,
              private route: ActivatedRoute,
              private eventService: EventService,
              private authService: AuthenticationService,
              private userService: UserService) { }
  
  eventID: number;
  event: Event = {} as Event;
  width: number;
  height: number;
  loggedUser: User = {} as User;
  role: boolean;
  isLogged: boolean;
  flag: boolean = false;
  lightC: boolean = false;
  newTime: Date;

  @ViewChild('myCanvas', {static: true}) myCanvas: ElementRef<HTMLCanvasElement>;
  public ctx: CanvasRenderingContext2D;

  ngOnInit() {

    this.isLogged = this.authService.isLoggedIn();
    if(this.isLogged == true){
      if(this.authService.getCurrentUser()){
        let role = this.authService.getCurrentUser().roles[0];
        if(role == "ROLE_ADMIN"){
          this.role = true;
        }else{
          this.role = false
        }
        let logged = this.authService.getCurrentUser().email;
        this.userService.getByEmail(logged).subscribe(data => {
          this.loggedUser = data;
        });
      }else{
        this.userService.getFacebookUser().subscribe(data => {
          this.loggedUser = data;
          this.role = false;
        });
      }
    }

    this.route.params.subscribe(params =>{
      this.eventID = params["id"];
    });

    this.event.facultyDTO = {} as Faculty;
    this.event.facultyDTO.universityDTO = {} as University;
    this.event.facultyDTO.universityDTO.placeDTO = {} as Place;
    this.eventService.getOneById(this.eventID).subscribe(data => {
      this.event = data;
      this.newTime = this.event.liveDuration;
    });

    this.ctx = this.myCanvas.nativeElement.getContext('2d');
    this.show();
  }

  controlLive(){
    document.getElementById("status").innerHTML = "Loading...";
    if(this.flag == false){
      this.liveService.startLiveStream().subscribe(data => {});
        this.sleep(4500);
        this.show();
     
        this.event.liveDuration = new Date();
        this.eventService.update(this.event).subscribe(res=>{
          this.newTime = res.liveDuration;
        });

    } else {
      this.liveService.stopLiveStream().subscribe(data => {});
      document.getElementById("status").innerHTML = "The live stream has just finished.";
      this.flag = false;
      this.lightC=false;
      if(this.role == true)
        document.getElementById("buttonSS").innerHTML = "START";
    }
  }

  show(){
    this.width = 1200;
    this.height = 600;
    let uri = "ws://" + window.location.hostname + ":8585";
    const socket = new WebSocket(uri);
    socket.addEventListener('open', (e) => {
        document.getElementById("status").innerHTML = "You are successfully watching the live stream.";
        this.flag=true;
        this.lightC=true;
        if(this.role == true)
          document.getElementById("buttonSS").innerHTML = "STOP";

    });
    socket.addEventListener('message', (e) => {
        
        let image = new Image();
        image.src = URL.createObjectURL(e.data);
        image.addEventListener("load", (e) => {
            this.ctx.drawImage(image, 0, 0, this.width, this.height);
        });
    });
    
  }


  sleep(mil){
    const dateN = Date.now();
    let trenutno= null;
    do {
      trenutno = Date.now();
    }while(trenutno-dateN < mil)
  }

  parseDate(date){
    let d = new Date(date);
    let sad = new Date();
    return this.msToTime(sad.getTime() - d.getTime());
  }

  pad(n) {
    return ('00' + n).slice(-2);
  }

  msToTime(s) {
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;
  
    return this.pad(hrs) + ':' + this.pad(mins) + ':' + this.pad(secs);
  }

  parseDate_v2(date){
    let parsed = moment(date).format('DD.MM.YYYY');
    return parsed;
  }

  dateParse(date){
    let parsed = moment(date).format('DD.MM.YYYY / hh:mm A');
    return parsed;
  }

  


}
