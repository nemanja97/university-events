import { Component, OnInit } from '@angular/core';
import { EventService } from '../services/Event/event.service';
import * as moment from 'moment';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.css']
})
export class LiveComponent implements OnInit {

  constructor(private eventService: EventService) { }

  events;
  message: boolean;

  refresh=window.setInterval(()=>{
    this.getEvents();
  },60000);

  ngOnInit() {
    this.getEvents();
  }

  ngOnDestroy(){
    window.clearInterval(this.refresh);
  }

  getEvents(){
    this.eventService.getLive().subscribe(data => {
        this.events = data;

        if(this.events.length == 0){
          this.message = true;
        }else{
          this.message = false;
        }
    });
  }

  parseDate(date){
    let parsed = moment(date).format('DD.MM.YYYY / hh:mm A');
    return parsed;
  }

  dateParse(date){
    let parsed = moment(date).format('DD.MM.YYYY');
    return parsed;
  }

}
