import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { OAuth2HandlerComponent } from './oauth2-handler/oauth2-handler.component';
import { AllUsersComponent } from './users/all-users/all-users.component';
import { CanActivateService } from './services/Login/Authentication/can-activate.service';
import { CanAdminService } from './services/Login/Authentication/can-admin.service';
import { UserComponent } from './users/user/user.component';
import { AddEventComponent } from './events/add-event/add-event.component';
import { LiveComponent } from './live/live.component';
import { EventComponent } from './events/event/event.component';
import { UsersForEventComponent } from './users/users-for-event/users-for-event.component';
import { EditEventComponent } from './events/edit-event/edit-event.component';
import { UserEventComponent } from './events/user-event/user-event.component';
import { LiveStreamComponent } from './live-stream/live-stream.component';


const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "register",
    component: RegisterComponent
  },
  {
    path: "oatuh2/redirect",
    component: OAuth2HandlerComponent
  },
  {
    path: "users",
    component: AllUsersComponent,
    canActivate: [CanActivateService, CanAdminService]
  },
  {
    path: "user/:id",
    component: UserComponent
  },
  {
    path: "add-event",
    component: AddEventComponent,
    canActivate: [CanActivateService, CanAdminService]
  },
  {
    path: "live",
    component: LiveComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "event/:id",
    component: EventComponent
  },
  {
    path: "confirmed-users/:id",
    component: UsersForEventComponent
  },
  {
    path: "edit-event/:id",
    component: EditEventComponent,
    canActivate: [CanActivateService, CanAdminService]
  },
  {
    path: "user-event/:id",
    component: UserEventComponent,
    canActivate: [CanActivateService]
  },
  {
    path: "live-stream/:id",
    component: LiveStreamComponent,
    canActivate: [CanActivateService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
