import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {FACEBOOK_AUTH_URL, ACCESS_TOKEN} from '../constants';
import Login from '../models/login';
import { AuthenticationService } from '../services/Login/Authentication/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,
              private authenticationServiceService: AuthenticationService) { }

  loginModel: Login={} as Login;
  messageAlert = false;

  ngOnInit() {

  }

  goToFacebook(){
    window.location.href = FACEBOOK_AUTH_URL;
  }

  submit(){
    if(this.loginModel.email != "" && this.loginModel.password != ""){
          this.authenticationServiceService.login(this.loginModel).subscribe(data =>{
            this.router.navigate(['/']);
          },error =>{
            this.messageAlert = true;
          });
    }else{
      this.messageAlert = true;
    }
  }


}
