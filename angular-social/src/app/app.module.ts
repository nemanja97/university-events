import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { OAuth2HandlerComponent } from './oauth2-handler/oauth2-handler.component';
import { UserComponent } from './users/user/user.component';
import { AllUsersComponent } from './users/all-users/all-users.component';
import { UsersForEventComponent } from './users/users-for-event/users-for-event.component';
import { EventComponent } from './events/event/event.component';
import { AddEventComponent } from './events/add-event/add-event.component';
import { EditEventComponent } from './events/edit-event/edit-event.component';
import { TokenInterceptorService } from './services/Login/Token-Interceptor/token-interceptor.service';
import { LiveComponent } from './live/live.component';
import { FooterComponent } from './footer/footer.component';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { UserEventComponent } from './events/user-event/user-event.component';
import { LiveStreamComponent } from './live-stream/live-stream.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    OAuth2HandlerComponent,
    UserComponent,
    AllUsersComponent,
    UsersForEventComponent,
    EventComponent,
    AddEventComponent,
    EditEventComponent,
    LiveComponent,
    FooterComponent,
    UserEventComponent,
    LiveStreamComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' 
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
