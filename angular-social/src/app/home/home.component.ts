import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/Login/Authentication/authentication.service';
import { Router } from '@angular/router';
import { EventService } from '../services/Event/event.service';
import * as moment from 'moment';
import { HttpResponse } from '@angular/common/http';
import Event from '../models/event';
import { UserService } from '../services/User/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authenticationServiceService: AuthenticationService,
              private router: Router,
              private eventService: EventService,
              private userService: UserService) { }

  isLogged;
  events: Event[];
  sumPages;
  activatedPage = 0;
  text: string = "";
  searchCondition: boolean = false;
  sortCondition: boolean = false;
  orderBy;
  orderCondition: string;
  reload: boolean = false;
  loggedUser: any;

  ngOnInit() {
    this.eventService.getAll(0,5).subscribe(data => {
      this.events = data.body;
      this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i);
    });

  }

  nextPage(event){
    let numberOfPage = event.target.name;
    document.getElementById(this.activatedPage.toString()).classList.remove("active");
    this.activatedPage = numberOfPage;

    if(this.searchCondition == false && this.sortCondition == false){
      this.eventService.getAll(numberOfPage,5).subscribe((data:HttpResponse<any>) => {
        this.events = data.body;
        this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
      });
    }else if(this.searchCondition == true && this.sortCondition == false){
      this.eventService.search(this.text, numberOfPage, 5).subscribe((data:HttpResponse<any>) => {
        this.events = data.body;
        this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
      });
    }else if(this.sortCondition == true && this.searchCondition ==false){
      this.eventService.getSortUsers(this.orderBy, this.orderCondition, numberOfPage, 5).subscribe((data:HttpResponse<any>) => {
        this.events = data.body;
        this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
      });
    }

    document.getElementById(this.activatedPage.toString()).classList.add("active");
  }

  parseDate(date){
    let parsed = moment(date).format('DD.MM.YYYY / hh:mm A');
    return parsed;
  }

  dateParse(date){
    let parsed = moment(date).format('DD.MM.YYYY');
    return parsed;
  }

  search(){
    if(this.text != ""){
      this.eventService.search(this.text, 0, 5).subscribe(result => {
        this.events = result.body;
        this.sumPages = Array(Number.parseInt(result.headers.get("totalPages"))).fill(0).map((x,i)=>i);
        this.searchCondition = true;
      }, error => {
        this.text = "";
        console.log(error);
      });
    }else{
      console.log("Empty");
    }
    this.reload = true;

  }

  reloadAll(){
    this.reload = false;
    this.ngOnInit();
  }

  sort(){
    if(this.orderBy == null || this.orderCondition == ""){
      alert("Sort fields must be filled!");
    }else{
      this.eventService.getSortUsers(this.orderBy, this.orderCondition, 0, 5).subscribe(data => {
        this.events = data.body;
        this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i);
        this.sortCondition = true;
      });
    }

  }

}
