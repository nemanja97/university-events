export default interface Place{
    id: number,
    name: string,
    zipCode: string
}