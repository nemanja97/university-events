import Place from './place';

export default interface University {
    id: number,
    name: string,
    address: string,
    email: string,
    phoneNumber: string,
    placeDTO: Place
}