import Event from './event';
import User from './user';

export default interface UserGoingToEvent {
    id: number,
    deleted: boolean,
    eventDTO: Event,
    userDTO: User
}