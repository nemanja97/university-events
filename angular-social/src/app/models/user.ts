import Place from './place';

export default interface User{
    id: number,
    username: string,
    password?: string,
    firstName: string,
    lastName: string,
    email: string,
    imageUrl: string,
    emailVerified: boolean,
    provider: string,
    role: string,
    deleted: boolean,
    providerId: string,
    blocked: boolean,
    placeDTO: Place
}