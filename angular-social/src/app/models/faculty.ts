import University from './university';

export default interface Faculty {
    id: number,
    name: string,
    address: string,
    universityDTO: University
}