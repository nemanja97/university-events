import Faculty from './faculty';

export default interface Event {
    id: number,
    title: string,
    description: string,
    date: string,
    endTime: string,
    coverPhoto: string,
    numberOfConfirmedArrivals: number,
    instructor: string,
    numberOfLike: number,
    numberOfDislike: number,
    dateOfCreation: Date,
    deleted: boolean,
    isLive: boolean,
    liveDuration: Date,
    facultyDTO: Faculty
}