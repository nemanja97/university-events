import User from './user';
import Event from './event';

export default interface Comment {
    id: number,
    content: string,
    date: Date,
    deleted: boolean,
    eventDTO: Event,
    userDTO: User
}