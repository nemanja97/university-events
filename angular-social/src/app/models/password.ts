import User from './user';

export default interface Password{
    password: string,
    userDTO: User
}