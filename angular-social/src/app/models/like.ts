import User from './user';
import Event from './event';

export default interface Like {
    id: number,
    liked: boolean,
    deleted: boolean,
    userDTO: User,
    eventDTO: Event,
}