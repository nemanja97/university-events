import { TestBed } from '@angular/core/testing';

import { CanAdminService } from './can-admin.service';

describe('CanAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CanAdminService = TestBed.get(CanAdminService);
    expect(service).toBeTruthy();
  });
});
