import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CanAdminService implements CanActivate {

  constructor(private authenticationService:AuthenticationService, private router:Router) { }

  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;

  canActivate(next:ActivatedRouteSnapshot,state:RouterStateSnapshot):Observable<boolean> | Promise<boolean> |boolean{
    if(this.authenticationService.getCurrentUser().roles[0] == "ROLE_ADMIN"){
      return true;
    }
    else{
      this.router.navigate(["/"]);
      return false;
    }
  }

}
