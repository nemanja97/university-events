import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { JwtUtilsService } from "../Jwt-utils/jwt-utils.service";
import { Observable, observable } from "rxjs";
import { map, catchError } from "rxjs/operators";
import Login from 'src/app/models/login';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly loginPath = '/api/auth/login';

  constructor(private http: HttpClient, private jwtUtilsService: JwtUtilsService) { }

  login(loginModel:Login): Observable<boolean> {
    var headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.loginPath, loginModel, { headers }).pipe(
      map((res: any) => {
        let token = res && res['accessToken'];
        console.log(token);
        if (token) {
          localStorage.setItem("accessToken", token);
          localStorage.setItem('currentUser', JSON.stringify({
            email: loginModel.email,
            roles: this.jwtUtilsService.getRoles(token),
            accessToken: token
          }));

          return true;
        }
        else {
          return false;
        }
      })
      ,catchError((error: any) => {
        if (error.status === 400) {
          return Observable.throw("Ilegal error");
        }
        else {
          return Observable.throw(error.json().error || 'Server error');
        }
      }));
  }

  getToken(): String {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    var token = localStorage.getItem("accessToken");
    return token ? token : "";
  }

  logout(): void {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('currentUser');
  }

  isLoggedIn(): boolean {
    if (this.getToken() != '') return true;
    else return false;
  }

  getCurrentUser() {
    if (localStorage.currentUser) {
      return JSON.parse(localStorage.currentUser);
    }
    else {
      return undefined;
    }
  }

  

}
