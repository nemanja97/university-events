import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import UserGoingToEvent from 'src/app/models/userGoingToEvent';

@Injectable({
  providedIn: 'root'
})
export class UserGoingToEventService {

  constructor(private http: HttpClient) { }

  private readonly url = "api/confirmEvent";

  getAll():Observable<UserGoingToEvent[]>{
    return this.http.get<UserGoingToEvent[]>(this.url);
  }

  getOneById(id: number):Observable<UserGoingToEvent>{
    return this.http.get<UserGoingToEvent>(this.url + `/${id}`);
  }

  getAllByEventId(id: number):Observable<UserGoingToEvent[]>{
    return this.http.get<UserGoingToEvent[]>(this.url + `/event/${id}`);
  }

  create(userGoingToEvent: any):Observable<UserGoingToEvent>{
    return this.http.post<UserGoingToEvent>(this.url, userGoingToEvent);
  }

  update(userGoingToEvent: any):Observable<UserGoingToEvent>{
    return this.http.put<UserGoingToEvent>(this.url + `/${userGoingToEvent.id}`, userGoingToEvent);
  }

  getByEventIdAndUserId(eventId: number, userId: number):Observable<UserGoingToEvent>{
    return this.http.get<UserGoingToEvent>(this.url + `/userConfirmed/${eventId}/${userId}`);
  }

  delete(id: number): any{
    return this.http.delete(this.url + "/" + id);
  }

  search(text: string):Observable<UserGoingToEvent[]>{
    return this.http.get<UserGoingToEvent[]>(this.url + `/search/${text}`);
  }

  searchByEvent(text: string):Observable<UserGoingToEvent[]>{
    return this.http.get<UserGoingToEvent[]>(this.url + `/searchByEvent/${text}`);
  }

  getAllByUserId(id: number):Observable<UserGoingToEvent[]>{
    return this.http.get<UserGoingToEvent[]>(this.url + `/user/${id}`);
  }

}
