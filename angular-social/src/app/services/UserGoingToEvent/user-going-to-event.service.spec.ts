import { TestBed } from '@angular/core/testing';

import { UserGoingToEventService } from './user-going-to-event.service';

describe('UserGoingToEventService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserGoingToEventService = TestBed.get(UserGoingToEventService);
    expect(service).toBeTruthy();
  });
});
