import { TestBed } from '@angular/core/testing';

import { LiveStreamService } from './live-stream.service';

describe('LiveStreamService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LiveStreamService = TestBed.get(LiveStreamService);
    expect(service).toBeTruthy();
  });
});
