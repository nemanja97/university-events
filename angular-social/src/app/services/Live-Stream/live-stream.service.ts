import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LiveStreamService {

  constructor(private http:HttpClient) { }

  private readonly url = "http://localhost:8080/api/live-stream";

  startLiveStream():any{
    return this.http.get(this.url + `/start`);
  }

  stopLiveStream():any{
    return this.http.get(this.url + `/stop`);
  }
}
