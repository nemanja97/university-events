import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import Event from 'src/app/models/event';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }

  private readonly url = "/api/events";

  getLive():any {
    return this.http.get(this.url + `/live`);
  }

  getAll(pageNumber:number,size:number):any {
    return this.http.get(this.url+`?page=${pageNumber}&size=${size}`,{observe:"response"});
  }

  getOneById(id: number):Observable<Event>{
    return this.http.get<Event>(this.url+`/${id}`);
  }

  create(event: any):Observable<Event>{
    return this.http.post<Event>(this.url, event);
  }
  
  update(event: any):Observable<Event>{
    return this.http.put<Event>(this.url + `/${event.id}`, event);
  }

  uploadPhoto(file: File):any{
    let formdata = new FormData();
    formdata.append('file',file);
    return this.http.post(this.url+`/upload`, formdata);
  }

  search(text: string, pageNumber:number, size:number):any{
    return this.http.get(this.url + `/search/${text}` + `?page=${pageNumber}&size=${size}`, {observe:"response"});
  }

  delete(id: number):any {
    return this.http.delete(this.url+`/${id}`);
  }

  getSortUsers(first_term: string, second_term: string, pageNumber:number, size:number):any{
    return this.http.get(this.url + `/sort/${first_term}/${second_term}`+ `?page=${pageNumber}&size=${size}`, {observe:"response"});
  }

}
