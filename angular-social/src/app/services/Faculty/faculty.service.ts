import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Faculty from 'src/app/models/faculty';

@Injectable({
  providedIn: 'root'
})
export class FacultyService {

  constructor(private http: HttpClient) { }

  private readonly url="/api/faculties";

  getAll():Observable<Faculty[]>{
    return this.http.get<Faculty[]>(this.url);
  }

}
