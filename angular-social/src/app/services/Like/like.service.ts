import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import Like from 'src/app/models/like';

@Injectable({
  providedIn: 'root'
})
export class LikeService {

  constructor(private http: HttpClient) { }

  private readonly url = "/api/likes";

  getByEventIdAndUserId(eventId: number, userId: number):Observable<Like>{
    return this.http.get<Like>(this.url + `/userLike/${eventId}/${userId}`);
  }

  createLike(like: any):Observable<Like>{
    return this.http.post<Like>(this.url + `/like`, like);
  }

  createDislike(dislike: any):Observable<Like>{
    return this.http.post<Like>(this.url + `/dislike`, dislike);
  }

  changeLike(like: any):Observable<Like>{
    return this.http.put<Like>(this.url + `/changeLike/${like.id}`, like);
  }

  changeDislike(dislike: any):Observable<Like>{
    return this.http.put<Like>(this.url +  `/changeDislike/${dislike.id}`, dislike);
  }

}
