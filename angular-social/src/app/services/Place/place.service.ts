import { Injectable } from '@angular/core';
import Place from 'src/app/models/place';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  constructor(private http: HttpClient) { }

  private readonly url="/api/places";

  getAll():Observable<Place[]>{
    return this.http.get<Place[]>(this.url);
  }

}
