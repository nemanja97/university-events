import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import Comment from 'src/app/models/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  private readonly url = "/api/comments";

  getOneById(id: number):Observable<Comment>{
    return this.http.get<Comment>(this.url + `/${id}`);
  }

  getAllByEventId(id: number):Observable<Comment[]>{
    return this.http.get<Comment[]>(this.url + `/event/${id}`);
  }

  create(comment: any):Observable<Comment>{
    return this.http.post<Comment>(this.url, comment);
  }

  update(comment: any):Observable<Comment>{
    return this.http.put<Comment>(this.url + `/${comment.id}`, comment);
  }

  delete(id: number):any {
    return this.http.delete(this.url+`/${id}`);
  }

}
