import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import User from 'src/app/models/user';
import Password from 'src/app/models/password';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  private readonly authURL = "/api/auth";
  private readonly userURL = "/api/users";

  create(user: any):Observable<User>{
    return this.http.post<User>(this.authURL + `/signup`, user);
  }

  uploadPhoto(file: File):any{
    let formdata = new FormData();
    formdata.append('file',file);
    return this.http.post(this.authURL + `/upload`, formdata);
  }

  getByEmail(email: string):Observable<User>{
    return this.http.get<User>(this.authURL + `/logged/${email}`);
  }

  getOneById(id: number):Observable<User>{
    return this.http.get<User>(this.authURL + `/${id}`);
  }

  getAll(pageNumber:number,size:number):any {
    return this.http.get(this.authURL+`?page=${pageNumber}&size=${size}`,{observe:"response"});
  }

  update(user: any):Observable<User>{
    return this.http.put<User>(this.authURL + `/${user.id}`, user);
  }

  search(text: string, pageNumber:number, size:number):any{
    return this.http.get(this.authURL + `/search/${text}` + `?page=${pageNumber}&size=${size}`, {observe:"response"});
  }

  delete(id: number):any {
    return this.http.delete(this.authURL+`/${id}`);
  }

  changePassword(password: any):Observable<Password>{
    return this.http.put<Password>(this.authURL + `/change-password`, password);
  }

  getFacebookUser():any{
    return this.http.get(this.userURL + `/user/me`);
  }

  getSortUsers(first_term: string, second_term: string, pageNumber:number, size:number):any{
    return this.http.get(this.authURL + `/sort/${first_term}/${second_term}`+ `?page=${pageNumber}&size=${size}`, {observe:"response"});
  }

}
