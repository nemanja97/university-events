import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/Login/Authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EventService } from 'src/app/services/Event/event.service';
import { UserService } from 'src/app/services/User/user.service';
import { UserGoingToEventService } from 'src/app/services/UserGoingToEvent/user-going-to-event.service';
import * as moment from 'moment';
import { HttpResponse } from '@angular/common/http';
import UserGoingToEvent from 'src/app/models/userGoingToEvent';

@Component({
  selector: 'app-user-event',
  templateUrl: './user-event.component.html',
  styleUrls: ['./user-event.component.css']
})
export class UserEventComponent implements OnInit {

  constructor(private authenticationServiceService: AuthenticationService,
              private router: Router,
              private eventService: EventService,
              private userService: UserService,
              private userGoingToEventService: UserGoingToEventService,
              private route: ActivatedRoute) { }


  isLogged;
  userWhoGoingToEvents: UserGoingToEvent[];
  text: string = "";
  orderBy;
  orderCondition: string;
  reload: boolean = false;
  message: boolean = false;
  userID: number;

  ngOnInit() {

    this.route.params.subscribe(params =>{
      this.userID = params["id"];
    });

    this.userGoingToEventService.getAllByUserId(this.userID).subscribe(data => {
      this.userWhoGoingToEvents = data;

      if(this.userWhoGoingToEvents.length == 0){
        this.message = true;
      }

    });
    
  }

  parseDate(date){
    let parsed = moment(date).format('DD.MM.YYYY / hh:mm A');
    return parsed;
  }

  dateParse(date){
    let parsed = moment(date).format('DD.MM.YYYY');
    return parsed;
  }

  search(){
    if(this.text != ""){
      this.userGoingToEventService.searchByEvent(this.text).subscribe(result => {
        this.userWhoGoingToEvents = result;
      }, error => {
        this.text = "";
        console.log(error);
      });
    }else{
      console.log("Empty");
    }
    this.reload = true;

  }

  reloadAll(){
    this.reload = false;
    this.ngOnInit();
  }

  sort(){
    if(this.orderBy == null || this.orderCondition == ""){
      alert("You must select data from dropdown!");
    }else{
      if(this.orderCondition === "asc"){
        if(this.orderBy === "title"){
          this.userWhoGoingToEvents = this.userWhoGoingToEvents.sort((a,b) => a.eventDTO.title.localeCompare(b.eventDTO.title));
        }else if(this.orderBy === "date"){
          this.userWhoGoingToEvents = this.userWhoGoingToEvents.sort((a,b) => a.eventDTO.date.localeCompare(b.eventDTO.date));
        }else if(this.orderBy === "number_of_like"){
          this.userWhoGoingToEvents = this.userWhoGoingToEvents.sort((a,b) => a.eventDTO.numberOfLike.toString().localeCompare(b.eventDTO.numberOfLike.toString()));
        }else if(this.orderBy === "number_of_dislike"){
          this.userWhoGoingToEvents = this.userWhoGoingToEvents.sort((a,b) => a.eventDTO.numberOfDislike.toString().localeCompare(b.eventDTO.numberOfDislike.toString()));
        }else if(this.orderBy === "number_of_confirmed_arrivals"){
          this.userWhoGoingToEvents = this.userWhoGoingToEvents.sort((a,b) => a.eventDTO.numberOfConfirmedArrivals.toString().localeCompare(b.eventDTO.numberOfConfirmedArrivals.toString()));
        }
      }else if(this.orderCondition === "desc"){
        if(this.orderBy === "title"){
          this.userWhoGoingToEvents = this.userWhoGoingToEvents.sort((a,b) => b.eventDTO.title.localeCompare(a.eventDTO.title));
        }else if(this.orderBy === "date"){
          this.userWhoGoingToEvents = this.userWhoGoingToEvents.sort((a,b) => b.eventDTO.date.localeCompare(a.eventDTO.date));
        }else if(this.orderBy === "number_of_like"){
          this.userWhoGoingToEvents = this.userWhoGoingToEvents.sort((a,b) => b.eventDTO.numberOfLike.toString().localeCompare(a.eventDTO.numberOfLike.toString()));
        }else if(this.orderBy === "number_of_dislike"){
          this.userWhoGoingToEvents = this.userWhoGoingToEvents.sort((a,b) => b.eventDTO.numberOfDislike.toString().localeCompare(a.eventDTO.numberOfDislike.toString()));
        }else if(this.orderBy === "number_of_confirmed_arrivals"){
          this.userWhoGoingToEvents = this.userWhoGoingToEvents.sort((a,b) => b.eventDTO.numberOfConfirmedArrivals.toString().localeCompare(a.eventDTO.numberOfConfirmedArrivals.toString()));
        }      
      }
    }

  }

}
