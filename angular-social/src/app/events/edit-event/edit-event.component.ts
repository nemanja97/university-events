import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from 'src/app/services/Event/event.service';
import { FacultyService } from 'src/app/services/Faculty/faculty.service';
import Faculty from 'src/app/models/faculty';
import Event from 'src/app/models/event';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private eventService: EventService,
              private facultyService: FacultyService,
              private router: Router) { }

  eventID: number;
  event: Event = {} as Event;
  faculties: Faculty[];
  messageAlert: boolean = false; 
  date;
  endDate;

  ngOnInit() {

    this.route.params.subscribe(params =>{
      this.eventID = params["id"];
    });

    this.event.facultyDTO = {} as Faculty;
    this.eventService.getOneById(this.eventID).subscribe(data => {
      this.event = data;
    });

    this.facultyService.getAll().subscribe(data => {
      this.faculties = data;
    });

  }

  submit(){
    let dateNow = new Date();
    this.date = new Date(this.date);
    this.endDate = new Date(this.endDate);
    if(this.date >= dateNow && this.endDate > this.date){
      this.event.date = this.date;
      this.event.endTime = this.endDate;
      this.event.facultyDTO = this.faculties.find(x => x.name == this.event.facultyDTO.name);
      this.eventService.update(this.event).subscribe( result => {
        this.router.navigate(["/event", result.id]);
      }, error => {
        this.messageAlert = true;
      });
    }else{
      this.messageAlert = true;
    }
  }

  parseDate(date){
    let parsed = moment(date).format('DD.MM.YYYY / hh:mm A');
    return parsed;
  }

}
