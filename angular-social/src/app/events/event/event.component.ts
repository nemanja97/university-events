import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';
import { EventService } from 'src/app/services/Event/event.service';
import { CommentService } from 'src/app/services/Comment/comment.service';
import { AuthenticationService } from 'src/app/services/Login/Authentication/authentication.service';
import { UserService } from 'src/app/services/User/user.service';
import { UserGoingToEventService } from 'src/app/services/UserGoingToEvent/user-going-to-event.service';
import { LikeService } from 'src/app/services/Like/like.service';
import User from 'src/app/models/user';
import UserGoingToEvent from 'src/app/models/userGoingToEvent';
import Like from 'src/app/models/like';
import Faculty from 'src/app/models/faculty';
import Event from 'src/app/models/event';
import University from 'src/app/models/university';
import Place from 'src/app/models/place';
import Comment from 'src/app/models/comment';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private eventService: EventService,
              private commentService: CommentService,
              private authService: AuthenticationService,
              private userService: UserService,
              private userGoingService: UserGoingToEventService,
              private likeService: LikeService,
              private router: Router) { }

  logged;
  role: boolean;
  isLogged: boolean;
  eventID: number;
  event: Event = {} as Event;
  messageAlert: boolean = false;
  user: User = {} as User;
  comments: Comment[];
  commentForm: boolean = false;
  editFormComment: boolean = false;
  commentObject: Comment = {} as Comment; // comment object for create
  comment: Comment = {} as Comment; // comment object for edit
  messageBlock: boolean = false;
  dateCondition: boolean = false;
  numberOfComments;

  userGoingToEventObject: UserGoingToEvent = {} as UserGoingToEvent;
  showButtonGo: boolean;
  foundUser: UserGoingToEvent = {} as UserGoingToEvent;

  likeObject: Like = {} as Like;
  likeButton: boolean = false;
  dislikeButton: boolean = false;
  foundLikeEvent: Like = {} as Like; // found like for event
  buttonEventLikeMessage: boolean = false;

  public popoverTitle: string = 'Delete event';
  public popoverMessage: string = 'Are you sure you want to delete this event?';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  ngOnInit() {

    this.route.params.subscribe(params =>{
      this.eventID = params["id"];
    });

    this.event.facultyDTO = {} as Faculty;
    this.event.facultyDTO.universityDTO = {} as University;
    this.event.facultyDTO.universityDTO.placeDTO = {} as Place;
    this.eventService.getOneById(this.eventID).subscribe(data => {
      this.event = data;
    });

    this.isLogged = this.authService.isLoggedIn();
    if(this.isLogged == true){
      if(this.authService.getCurrentUser()){
        let role = this.authService.getCurrentUser().roles[0];
        if(role == "ROLE_ADMIN"){
          this.role = true;
        }else{
          this.role = false
        }

        this.logged = this.authService.getCurrentUser().email;
        this.commentForm = true;

        this.userService.getByEmail(this.logged).subscribe( data =>{
          this.user = data;
          this.userGoingService.getByEventIdAndUserId(this.eventID, this.user.id).subscribe( result =>{
            this.foundUser = result;
            this.showButtonGo = true;
          },error => {
            this.showButtonGo = false;
          });
          this.likeService.getByEventIdAndUserId(this.eventID, this.user.id).subscribe( data => {
            this.foundLikeEvent = data;
            if(this.foundLikeEvent.liked == true){
              this.likeButton = true;
            }else if(this.foundLikeEvent.liked == false){
              this.dislikeButton = true;
            }
          }, error => {
            this.likeButton = false;
            this.dislikeButton = false;
          });
        });
      }else{
        this.userService.getFacebookUser().subscribe(data => {
          this.user = data;
          this.role = false;
          this.commentForm = true;
          this.userGoingService.getByEventIdAndUserId(this.eventID, this.user.id).subscribe( result =>{
            this.foundUser = result;
            this.showButtonGo = true;
          },error => {
            this.showButtonGo = false;
          });
          this.likeService.getByEventIdAndUserId(this.eventID, this.user.id).subscribe( data => {
            this.foundLikeEvent = data;
            if(this.foundLikeEvent.liked == true){
              this.likeButton = true;
            }else if(this.foundLikeEvent.liked == false){
              this.dislikeButton = true;
            }
          }, error => {
            this.likeButton = false;
            this.dislikeButton = false;
          });
        });
      }
    }

    this.commentService.getAllByEventId(this.eventID).subscribe( result => {
      this.comments = result;
      this.numberOfComments = this.comments.length;
    });

  }

  parseDate(date){
    let parsed = moment(date).format('DD.MM.YYYY');
    return parsed;
  }

  dateParse(date){
    let parsed = moment(date).format('DD.MM.YYYY / hh:mm A');
    return parsed;
  }

  timeParse(date){
    let parsed = moment(date).format('hh:mm A');
    return parsed;
  }

  createComment(){
    if(this.user.blocked == false){
      if(this.commentObject.content != null){
        this.commentObject.date = new Date();
        this.commentObject.eventDTO = this.event;
        this.commentObject.userDTO = this.user;
    
        this.commentService.create(this.commentObject).subscribe( result => {
          let newComments=[result,...this.comments];
          this.comments=newComments;
          this.numberOfComments = this.comments.length;
        }, error => {
          this.messageAlert = true;
        }); 
        this.refreshComment();
      }else{
        this.messageAlert = true;
      }
    }else{
      this.messageBlock = true;
    }
  }

  refreshComment(){
    this.commentObject = {} as Comment;
    this.messageAlert = false;
  }

  openEditForm(event){
    let commentID = Number.parseInt(event.target.name);
    this.commentService.getOneById(commentID).subscribe( data => {
      this.comment = data;
    });
    this.editFormComment = true;
  }

  closeEditFormComment(){
    this.editFormComment = false;
  }

  clickSubmitComment(){
    this.comment.date = new Date();
    this.commentService.update(this.comment).subscribe( result => {
      let index = this.comments.findIndex( item => item.id == result.id);
      this.comments[index] = result;
    });
    this.closeEditFormComment();

  }

  deleteComment(event){
    let commentID = event.target.name;
    this.commentService.delete(commentID).subscribe(result => {
      this.commentService.getAllByEventId(this.eventID).subscribe( result => {
        this.comments = result;
        this.numberOfComments = this.comments.length;
      });
    });
  }

  goToEvent(){
    let dateNow = new Date();
    let dateEvent = new Date(this.event.date);
    console.log(dateNow);
    console.log(dateEvent);
    if(dateNow > dateEvent){
      this.dateCondition = true;
    }else{
      if(this.isLogged == true){
        this.userGoingToEventObject.eventDTO = this.event;
        this.userGoingToEventObject.userDTO = this.user;
  
        this.userGoingService.create(this.userGoingToEventObject).subscribe( result => {
          this.showButtonGo = true;
          this.event = result.eventDTO;
          this.foundUser = result;
        })
      }
    }
  }

  notGoToEvent(){
    let dateNow = new Date();
    let dateEvent = new Date(this.event.date);
    if(dateNow > dateEvent){
      this.dateCondition = true;
    }else{
      this.userGoingService.delete(this.foundUser.id).subscribe( data => {
        this.showButtonGo = false;
        this.event.numberOfConfirmedArrivals=data;
      });
    }
  }

  like(){
    if(this.isLogged == true){
        if(Object.keys(this.foundLikeEvent).length === 0){
          this.likeObject.eventDTO = this.event;
          this.likeObject.userDTO = this.user;
          this.likeService.createLike(this.likeObject).subscribe(data => {
            this.event = data.eventDTO;
            this.likeButton = true;
            this.foundLikeEvent = data; 
          });
        }else if(Object.keys(this.foundLikeEvent).length > 0 && this.foundLikeEvent.liked == false){
          this.likeService.getByEventIdAndUserId(this.eventID, this.user.id).subscribe(data => {
            this.foundLikeEvent = data;
              this.likeService.changeLike(this.foundLikeEvent).subscribe(data => {
                this.event = data.eventDTO;
                this.dislikeButton = false;
                this.likeButton = true;
                this.foundLikeEvent = data;
              });
          });
        }
    }else{
      this.buttonEventLikeMessage = true;
    }

  }

  dislike(){
    if(this.isLogged == true){
        if(Object.keys(this.foundLikeEvent).length === 0){
          this.likeObject.eventDTO = this.event;
          this.likeObject.userDTO = this.user;
          this.likeService.createDislike(this.likeObject).subscribe(data => {
            this.event = data.eventDTO;
            this.dislikeButton = true;
            this.foundLikeEvent = data;
          }); 
        }else if(Object.keys(this.foundLikeEvent).length > 0 && this.foundLikeEvent.liked == true){
          this.likeService.getByEventIdAndUserId(this.eventID, this.user.id).subscribe(data => {
            this.foundLikeEvent = data;
              this.likeService.changeDislike(this.foundLikeEvent).subscribe(data => {
                this.event = data.eventDTO;
                this.likeButton = false;
                this.dislikeButton = true;
                this.foundLikeEvent = data;
              });
          });
        }
    }else{
      this.buttonEventLikeMessage = true;
    }

  }

  deleteEvent(){
    this.eventService.delete(this.eventID).subscribe(result => {
      this.router.navigate(["/"]);
    });
  }

}
