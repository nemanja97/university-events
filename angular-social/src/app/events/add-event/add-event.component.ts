import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EventService } from 'src/app/services/Event/event.service';
import { FacultyService } from 'src/app/services/Faculty/faculty.service';
import Event from 'src/app/models/event';
import Faculty from 'src/app/models/faculty';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.css']
})
export class AddEventComponent implements OnInit {

  constructor(private facultyService: FacultyService,
              private router: Router,
              private eventService: EventService) { }

  event: Event = {} as Event;
  faculties: Faculty[];
  messageAlert = false;
  messageAlert2 = false;
  file: File;
  date;
  endDate;

  ngOnInit() {
    this.event.facultyDTO = {} as Faculty;
    this.facultyService.getAll().subscribe(data => {
      this.faculties = data;
    });
  }

  selectFile(event) {
    this.file = event.target.files.item(0);
  }

  submit(){
    let dateNow = new Date();
    this.date = new Date(this.date);
    this.endDate = new Date(this.endDate);
    if(this.date >= dateNow && this.endDate > this.date){
        this.eventService.uploadPhoto(this.file).subscribe( response =>{
          this.event.coverPhoto = response.path;
          this.event.dateOfCreation = new Date();
          this.event.date = this.date;
          this.event.endTime = this.endDate;
          this.event.facultyDTO = this.faculties.find(x => x.name == this.event.facultyDTO.name);
          this.eventService.create(this.event).subscribe( data =>{
            this.router.navigate(["/"]);
          });
        });
    }else{
      this.messageAlert = true;
    }
  }

}
