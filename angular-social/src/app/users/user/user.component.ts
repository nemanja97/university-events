import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/User/user.service';
import { AuthenticationService } from 'src/app/services/Login/Authentication/authentication.service';
import { PlaceService } from 'src/app/services/Place/place.service';
import { Router, ActivatedRoute } from '@angular/router';
import User from 'src/app/models/user';
import Place from 'src/app/models/place';
import Password from 'src/app/models/password';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private route: ActivatedRoute, 
              private userService: UserService,
              private authService: AuthenticationService,
              private placeService: PlaceService,
              private router: Router) { }

  loggedUser: User = {} as User;
  role: boolean;
  isLogged: boolean;
  userID: number;
  user: User = {} as User;
  editProfileForm: boolean = false;
  listOfPlaces: Place[];
  changePasswordForm: boolean = false;
  passwordObject: Password = {} as Password;
  messageBlock: boolean = false;
  facebook: boolean = false;

  public popoverTitle: string = 'Delete user';
  public popoverMessage: string = 'Are you sure you want to delete this user?';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  ngOnInit() {

    this.route.params.subscribe(params =>{
      this.userID = params["id"];
    });

    this.user.placeDTO = {} as Place;
    this.userService.getOneById(this.userID).subscribe( data => {
      this.user = data;
      if(this.user.blocked == true){
        this.messageBlock = true;
      }
    })

    this.isLogged = this.authService.isLoggedIn();
    if(this.isLogged == true){
      if(this.authService.getCurrentUser()){
        let role = this.authService.getCurrentUser().roles[0];
        if(role == "ROLE_ADMIN"){
          this.role = true;
        }else{
          this.role = false
        }
        this.facebook = false;
        let logged = this.authService.getCurrentUser().email;
        this.userService.getByEmail(logged).subscribe(data => {
          this.loggedUser = data;
        });
      }else{
        this.userService.getFacebookUser().subscribe(data => {
          this.loggedUser = data;
          this.role = false;
          this.facebook = true;
        });
      }
    }

  }

  openEditProfileForm(){
    this.editProfileForm = true;
    this.placeService.getAll().subscribe(data => {
      this.listOfPlaces = data;
    })
  }

  submit(){
    this.user.placeDTO = this.listOfPlaces.find(x => x.name == this.user.placeDTO.name);
    this.userService.update(this.user).subscribe( result => {
      this.editProfileForm = false;
    });
  }

  closeEditProfileForm(){
    this.editProfileForm = false;
  }

  openChangePasswordForm(){
    this.changePasswordForm = true;
  }

  submitNewPassword(){
    this.passwordObject.userDTO = this.user;
    this.userService.changePassword(this.passwordObject).subscribe(result => {
      this.changePasswordForm = false;
    });
  }

  closeChangePasswordForm(){
    this.changePasswordForm = false;
    this.passwordObject = {} as Password;
  }

  block(){
    this.user.blocked = true;
    this.userService.update(this.user).subscribe(data => {});
  }

  unblock(){
    this.user.blocked = false;
    this.userService.update(this.user).subscribe(data => {});
  }

  deleteProfile(){
    this.userService.delete(this.userID).subscribe(result => {
        this.router.navigate(["/users"]);
    });
  }

}
