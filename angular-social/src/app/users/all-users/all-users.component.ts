import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/User/user.service';
import { Router } from '@angular/router';
import User from 'src/app/models/user';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit {

  constructor(private router: Router,
              private userService: UserService) { }

  users: User[];
  sumPages;
  activatedPage = 0;
  serialNumber = 0;
  text: string = "";
  searchCondition: boolean = false;
  reload: boolean = false;
  orderBy: string;
  orderCondition: string;
  sortCondition: boolean = false;  

  ngOnInit() {

    this.userService.getAll(0,10).subscribe(data => {
      this.users = data.body;
      this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i);
    });

  }

  nextPage(event){
    let numberOfPage = event.target.name;
    document.getElementById(this.activatedPage.toString()).classList.remove("active");
    this.activatedPage = numberOfPage;

    if(this.searchCondition == false && this.sortCondition == false){
      this.userService.getAll(numberOfPage,10).subscribe((data:HttpResponse<any>) => {
        this.users = data.body;
        this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
      });
    }else if(this.searchCondition == true && this.sortCondition == false){
      this.userService.search(this.text, numberOfPage, 10).subscribe((data:HttpResponse<any>) => {
        this.users = data.body;
        this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
      });
    }else if(this.sortCondition == true && this.searchCondition == false){
      this.userService.getSortUsers(this.orderBy, this.orderCondition, numberOfPage, 10).subscribe((data:HttpResponse<any>) => {
        this.users = data.body;
        this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i); 
      });
    }

    document.getElementById(this.activatedPage.toString()).classList.add("active");
  }

  search(){
    if(this.text != ""){
      this.userService.search(this.text, 0, 10).subscribe(data => {
        this.users = data.body;
        this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i);
        this.searchCondition = true;
      }, error => {
        this.text = "";
      });
    }else{
      console.log("Empty!");
    }
    this.reload = true;
  }

  reloadAll(){
    this.reload = false;
    this.ngOnInit();
  }

  sort(){
    if(this.orderBy == null || this.orderCondition == ""){
      alert("Sort fields must be filled!");
    }else{
      this.userService.getSortUsers(this.orderBy, this.orderCondition, 0, 10).subscribe(data => {
        this.users = data.body;
        this.sumPages = Array(Number.parseInt(data.headers.get("totalPages"))).fill(0).map((x,i)=>i);
        this.sortCondition = true;
      });
    }
  }

}
