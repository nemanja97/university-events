import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersForEventComponent } from './users-for-event.component';

describe('UsersForEventComponent', () => {
  let component: UsersForEventComponent;
  let fixture: ComponentFixture<UsersForEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersForEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersForEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
