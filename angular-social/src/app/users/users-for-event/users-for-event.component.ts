import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserGoingToEventService } from 'src/app/services/UserGoingToEvent/user-going-to-event.service';
import UserGoingToEvent from 'src/app/models/userGoingToEvent';

@Component({
  selector: 'app-users-for-event',
  templateUrl: './users-for-event.component.html',
  styleUrls: ['./users-for-event.component.css']
})
export class UsersForEventComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private userGoingService: UserGoingToEventService) { }

  eventID: number;
  usersWhoGoToEvent: UserGoingToEvent[];
  message: boolean = false;
  text: string = "";
  reload: boolean = false;
  orderBy;
  orderCondition: string;

  ngOnInit() {

    this.route.params.subscribe(params =>{
      this.eventID = params["id"];
    });

    this.userGoingService.getAllByEventId(this.eventID).subscribe( data => {
      this.usersWhoGoToEvent = data;

      if(this.usersWhoGoToEvent.length == 0){
        this.message = true;
      }

    });

  }

  search(){
    if(this.text != ""){
      this.userGoingService.search(this.text).subscribe(data => {
        this.usersWhoGoToEvent = data;
      }, error => {
        this.text = "";
      })
    }else{
      console.log("Empty!");
    }
    this.reload = true;
  }

  reloadAll(){
    this.reload = false;
    this.ngOnInit();
  }

  sort(){
    if(this.orderBy == null || this.orderCondition == ""){
      alert("You must select data from dropdown!");
    }else{
      if(this.orderCondition === "asc"){
        if(this.orderBy === "firstName"){
          this.usersWhoGoToEvent = this.usersWhoGoToEvent.sort((a,b) => a.userDTO.firstName.localeCompare(b.userDTO.firstName));
        }else if(this.orderBy === "lastName"){
          this.usersWhoGoToEvent = this.usersWhoGoToEvent.sort((a,b) => a.userDTO.lastName.localeCompare(b.userDTO.lastName));
        }
      }else if(this.orderCondition === "desc"){
        if(this.orderBy === "firstName"){
          this.usersWhoGoToEvent = this.usersWhoGoToEvent.sort((a,b) => b.userDTO.firstName.localeCompare(a.userDTO.firstName));
        }else if(this.orderBy === "lastName"){
          this.usersWhoGoToEvent = this.usersWhoGoToEvent.sort((a,b) => b.userDTO.lastName.localeCompare(a.userDTO.lastName));
        }   
      }
    }

  }

}
