import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import User from '../models/user';
import { UserService } from '../services/User/user.service';
import Place from '../models/place';
import { PlaceService } from '../services/Place/place.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router,
              private userService: UserService,
              private placeService: PlaceService) { }

  userObject: User={} as User;
  messageAlert = false;
  listOfPlaces: Place[];
  file: File;
  sameUser = false;

  ngOnInit() {

    this.userObject.placeDTO = {} as Place;
    this.placeService.getAll().subscribe(data => {
      this.listOfPlaces = data;
    });

  }

  submit(){
    this.userService.getByEmail(this.userObject.email).subscribe(data => {
        this.sameUser = true;
    },e =>{
      if(e.status == 404){  
        this.userService.uploadPhoto(this.file).subscribe( response =>{ 
          this.userObject.imageUrl = response.path;
          this.userObject.placeDTO = this.listOfPlaces.find(x => x.name == this.userObject.placeDTO.name);
          this.userService.create(this.userObject).subscribe(data =>{
              this.router.navigate(["/"]);
          },error =>{
            this.messageAlert = true;
          });
        });
      }
    });
  }

  selectFile(event) {
    this.file = event.target.files.item(0);
  }

}
