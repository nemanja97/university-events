import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/Login/Authentication/authentication.service';
import { Router } from '@angular/router';
import User from '../models/user';
import { UserService } from '../services/User/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authenticationServiceService: AuthenticationService,
              private router: Router,
              private userService: UserService) { }

  isLogged:boolean;
  role;
  loggedUser: User={} as User;

  ngOnInit() {
    this.isLogged = this.authenticationServiceService.isLoggedIn();
    if(this.isLogged == true){

      if(this.authenticationServiceService.getCurrentUser()){
        let role = this.authenticationServiceService.getCurrentUser().roles[0];
        if(role == "ROLE_ADMIN"){
          this.role = true;
        }else{
          this.role = false
        }
  
        this.userService.getByEmail(this.authenticationServiceService.getCurrentUser().email).subscribe( data => {
          this.loggedUser = data;
        });
      }else{
        this.userService.getFacebookUser().subscribe(data => {
          this.loggedUser = data;
          this.role = false;
        });
      }

    }

  }

  loadHome(){
    this.router.navigate(["/"]);
  }

  logout(){
    this.authenticationServiceService.logout();
    this.isLogged = false;
    this.router.navigate(["/"]);
  }

}
