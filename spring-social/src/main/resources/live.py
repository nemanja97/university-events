import asyncio
import websockets
import multiprocessing
import cv2
from datetime import datetime as dt

PROCESSES = []
URL = 'C:\\Users\\natasa_gacic\\Desktop\\university-events\\spring-social\\src\\main\\resources\\c.txt'


def log(message):
    print("[LOG] " + str(dt.now()) + " - " + message)


def camera(man):
    log("Starting")
    vc = cv2.VideoCapture(0)

    if vc.isOpened():
        r, f = vc.read()
    else:
        r = False

    while r:
        cv2.waitKey(20)
        r, f = vc.read()
        f = cv2.resize(f, (640, 480))
        encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 65]
        man[0] = cv2.imencode('.jpg', f, encode_param)[1]


def socket(man):
    async def handler(websocket, path):
        log("Socket opened")
        try:
            while True:
                await asyncio.sleep(0.033)
                await websocket.send(man[0].tobytes())
        except websockets.exceptions.ConnectionClosed:
            log("Socket closed")

    log("Starting socket handler")
    start_server = websockets.serve(ws_handler=handler, host='0.0.0.0', port=8585)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()


def main():
    f = open(URL, "w")
    f.write("t")
    f.close()
    manager = multiprocessing.Manager()
    lst = manager.list()
    lst.append(None)
    socket_handler = multiprocessing.Process(target=socket, args=(lst,))
    camera_handler = multiprocessing.Process(target=camera, args=(lst,))

    PROCESSES.append(camera_handler)
    PROCESSES.append(socket_handler)

    for p in PROCESSES:
        p.start()
    while True:
        f = open(URL, "r")
        if "f" in f.read():
            for p in PROCESSES:
                p.terminate()
            break
        f.close()
        pass


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        for p in PROCESSES:
            p.terminate()
