INSERT INTO place (name, zip_code) VALUES ('None', '00000');
INSERT INTO place (name, zip_code) VALUES ('Novi Sad', '21000');
INSERT INTO place (name, zip_code) VALUES ('Belgrade', '11000');
INSERT INTO place (name, zip_code) VALUES ('Zrenjanin', '23000');
INSERT INTO place (name, zip_code) VALUES ('Valjevo', '14000');
INSERT INTO place (name, zip_code) VALUES ('Kraljevo', '36000');
INSERT INTO place (name, zip_code) VALUES ('Subotica', '24000');
INSERT INTO place (name, zip_code) VALUES ('Sremska Mitrovica', '22000');
INSERT INTO place (name, zip_code) VALUES ('Obrenovac', '11500');
INSERT INTO place (name, zip_code) VALUES ('Kragujevac', '34000');
INSERT INTO place (name, zip_code) VALUES ('Loznica', '15300');
INSERT INTO place (name, zip_code) VALUES ('Sombor', '25000');
INSERT INTO place (name, zip_code) VALUES ('Nis', '18000');
INSERT INTO place (name, zip_code) VALUES ('Pancevo', '26000');
INSERT INTO place (name, zip_code) VALUES ('Cacak', '32000');
INSERT INTO place (name, zip_code) VALUES ('Krusevac', '37000');
INSERT INTO place (name, zip_code) VALUES ('Novi Pazar', '36300');
INSERT INTO place (name, zip_code) VALUES ('Smederevo', '11300');
INSERT INTO place (name, zip_code) VALUES ('Leskovac', '16000');
INSERT INTO place (name, zip_code) VALUES ('Uzice', '31000');
INSERT INTO place (name, zip_code) VALUES ('Vranje', '17500');
INSERT INTO place (name, zip_code) VALUES ('Pozarevac', '12000');
INSERT INTO place (name, zip_code) VALUES ('Pirot', '183000');
INSERT INTO place (name, zip_code) VALUES ('Zajecar', '19000');
INSERT INTO place (name, zip_code) VALUES ('Kikinda', '23300');
INSERT INTO place (name, zip_code) VALUES ('Jagodina', '35000');
INSERT INTO place (name, zip_code) VALUES ('Vrsac', '26300');
INSERT INTO place (name, zip_code) VALUES ('Bor', '19210');
INSERT INTO place (name, zip_code) VALUES ('Prokuplje', '18400');

INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('mare27', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Marko', 'Markovic', 'mare@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user.png', false, 'ROLE_ADMIN', 'local', false, '', false, 2);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('pera42', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Petar', 'Maksimovic', 'pera@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user2.png', false, 'ROLE_ADMIN', 'local', false, '', false, 4);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Zivojin', 'Vasic', 'zdera@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Kojo', 'Vasic', 'joka@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Sojo', 'Vasic', 'soki@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Viko', 'Vasic', 'viko@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Zoran', 'Vasic', 'zola@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Mijolim', 'Vasic', 'mijo@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Zorkk', 'Vasic', 'zoki@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Jole', 'Vasic', 'jole@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Veran', 'Vasic', 'vera@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Caric', 'Vasic', 'car@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);
INSERT INTO users (username, password, first_name, last_name, email, image_url, email_verified, role, provider, deleted, provider_id, blocked, place_id) VALUES ('zdera89', '$2a$10$9tRKMJbGTYdHhqrAxHxVIeB6xxb5XJAFKs8/9i/YVbakNGbuj82r6', 'Aleksa', 'Vasic', 'aki@gmail.com', 'C:/Users/natasa_gacic/Desktop/university-events/files/users/user3.png', false, 'ROLE_USER', 'local', false, '', false, 6);


INSERT INTO university (name, address, email, phone_number, place_id) VALUES ('University of Novi Sad', 'Dr Zorana Djindjica 1', 'UNSinfo@uns.ac.rs', '+381 21 485-2000', 2);

INSERT INTO faculty (name, address, university_id) VALUES ('Faculty of Technical Sciences', 'Trg Dositeja Obradovica 6', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Faculty of Law', 'Trg Dositeja Obradovica 1', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Faculty of Sciences', 'Trg Dositeja Obradovica 3', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Faculty of Agriculture', 'Trg Dositeja Obradovica 8', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Faculty of Philosophy', 'Dr Zorana Djindjica 2', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Faculty of Sport and Physical Education', 'Lovcenska 16', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Faculty of Economics', 'Dr Sime Milosevica 16', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Faculty of Medicine', 'Hajduk Veljkova 3', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Academy of Arts', 'Djure Jaksica 7', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Academy of Technology', 'Bulevar Cara Lazara 1', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Faculty of Civil Engineering', 'Kozaracka 2a', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Faculty of Education', 'Podgoricka 4', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Technical faculty "Mihajlo Pupin"', 'Djure Djakovica bb', 1);
INSERT INTO faculty (name, address, university_id) VALUES ('Hungarian Language Teacher Training Faculty', 'Strosmajerova 11', 1);

INSERT INTO event (title, description, date, end_time, cover_photo, number_of_confirmed_arrivals, instructor, number_of_like, number_of_dislike, date_of_creation, deleted, is_live, live_duration, faculty_id) VALUES ('First event', 'The latest IT center in Novi Sad. Everything is possible.', '2019-07-11 16:40:00', '2019-07-11 20:40:00', 'C:/Users/natasa_gacic/Desktop/university-events/files/events/photo1.jpg', 2, 'Mladen Jovovic', 1, 0, '2019-06-11', false, false, null, 1);
INSERT INTO event (title, description, date, end_time, cover_photo, number_of_confirmed_arrivals, instructor, number_of_like, number_of_dislike, date_of_creation, deleted, is_live, live_duration, faculty_id) VALUES ('Second event', 'The latest IT center in Novi Sad. Everything is possible.', '2019-07-12 09:20:00', '2019-07-12 20:40:00', 'C:/Users/natasa_gacic/Desktop/university-events/files/events/photo2.jpg', 0, 'Mladen Jovovic', 0, 1, '2019-07-11', false, false, null, 2);
INSERT INTO event (title, description, date, end_time, cover_photo, number_of_confirmed_arrivals, instructor, number_of_like, number_of_dislike, date_of_creation, deleted, is_live, live_duration, faculty_id) VALUES ('Third event', 'The latest IT center in Novi Sad. Everything is possible.', '2019-07-13 15:38:00', '2019-07-13 20:40:00', 'C:/Users/natasa_gacic/Desktop/university-events/files/events/photo3.jpg', 0, 'Vladimir Kovacevic', 0, 0, '2019-08-11', false, false, null, 3);

INSERT INTO comment (content, date, deleted, user_id, event_id) VALUES ('Nothing special!', '2019-07-19', false, 1, 1);
INSERT INTO comment (content, date, deleted, user_id, event_id) VALUES ('Very good!', '2019-06-12', false, 2, 1);

INSERT INTO user_going_to_event (deleted, event_id, user_id) VALUES (false, 1, 1);
INSERT INTO user_going_to_event (deleted, event_id, user_id) VALUES (false, 1, 2);
INSERT INTO user_going_to_event (deleted, event_id, user_id) VALUES (false, 2, 3);

INSERT INTO like_dislike (is_liked, deleted, user_id, event_id) VALUE (true, false, 1, 1);
INSERT INTO like_dislike (is_liked, deleted, user_id, event_id) VALUE (false, false, 1, 2);