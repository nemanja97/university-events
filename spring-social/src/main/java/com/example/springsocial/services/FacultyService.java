package com.example.springsocial.services;

import com.example.springsocial.model.Faculty;
import com.example.springsocial.repository.FacultyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacultyService {

    @Autowired
    private FacultyRepository facultyRepository;

    public List<Faculty> findAll(){
        return facultyRepository.findAll();
    }

    public Faculty findOne(Long id){
        return facultyRepository.getOne(id);
    }

    public Faculty save(Faculty faculty){
        return facultyRepository.save(faculty);
    }

}
