package com.example.springsocial.services;

import com.example.springsocial.model.User;
import com.example.springsocial.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public Page<User> findAll(Pageable pageable){
        Page<User> users = userRepository.findAll(pageable);
        return users;
    }

    public User findById(Long id){
        return userRepository.getOne(id);
    }

    public User save(User user){
        return userRepository.save(user);
    }

    public User findUserByEmail(String email){
        return userRepository.findUserByEmail(email);
    }

    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public void saveFile(byte[] bytes, String path) throws IOException {
        Files.write(Paths.get(path), bytes);
    }

    public Page<User> search(String text, Pageable pageable){
        return userRepository.search(text, pageable);
    }

    public void remove(Long id){
        User user = userRepository.getOne(id);
        if (user != null){
            user.setDeleted(true);
            userRepository.save(user);
        }
    }

    public Page<User> findAllOrderByUsernameAsc(Pageable pageable){
        return userRepository.findAllByOrderByUsernameAsc(pageable);
    }

    public Page<User> findAllOrderByUsernameDesc(Pageable pageable){
        return userRepository.findAllByOrderByUsernameDesc(pageable);
    }

    public Page<User> findAllOrderByFirstNameAsc(Pageable pageable){
        return userRepository.findAllByOrderByFirstNameAsc(pageable);
    }

    public Page<User> findAllOrderByFirstNameDesc(Pageable pageable){
        return userRepository.findAllByOrderByFirstNameDesc(pageable);
    }

    public Page<User> findAllOrderByLastNameAsc(Pageable pageable){
        return userRepository.findAllByOrderByLastNameAsc(pageable);
    }

    public Page<User> findAllOrderByLastNameDesc(Pageable pageable){
        return userRepository.findAllByOrderByLastNameDesc(pageable);
    }

    public Page<User> findAllOrderByEmailAsc(Pageable pageable){
        return userRepository.findAllByOrderByEmailAsc(pageable);
    }

    public Page<User> findAllOrderByEmailDesc(Pageable pageable){
        return userRepository.findAllByOrderByEmailDesc(pageable);
    }

}
