package com.example.springsocial.services;

import com.example.springsocial.model.University;
import com.example.springsocial.repository.UniversityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UniversityService {

    @Autowired
    private UniversityRepository universityRepository;

    public List<University> findAll(){
        return universityRepository.findAll();
    }

    public University findById(Long id){
        return universityRepository.getOne(id);
    }

    public University save(University university){
        return universityRepository.save(university);
    }

}
