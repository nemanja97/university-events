package com.example.springsocial.services;

import com.example.springsocial.model.UserGoingToEvent;
import com.example.springsocial.repository.UserGoingToEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserGoingToEventService {

    @Autowired
    private UserGoingToEventRepository userGoingToEventRepository;

    public List<UserGoingToEvent> findAll(){
        return userGoingToEventRepository.findAll();
    }

    public UserGoingToEvent findById(Long id){
        return userGoingToEventRepository.getOne(id);
    }

    public UserGoingToEvent save(UserGoingToEvent userGoingToEvent){
        return userGoingToEventRepository.save(userGoingToEvent);
    }

    public List<UserGoingToEvent> findAllByEventId(Long id){
        return userGoingToEventRepository.findByEvent_Id(id);
    }

    public UserGoingToEvent findOneByEventIdAndUserId(Long eventId, Long userId){
        return userGoingToEventRepository.findByEvent_IdAndUser_Id(eventId, userId);
    }

    public void delete(Long id){
        userGoingToEventRepository.deleteById(id);
    }

    public List<UserGoingToEvent> search(String text){
        return userGoingToEventRepository.search(text);
    }

    public List<UserGoingToEvent> searchByEvent(String text){
        return userGoingToEventRepository.searchByEvent(text);
    }

    public List<UserGoingToEvent> findAllByUserID(Long id){
        return userGoingToEventRepository.findByUser_Id(id);
    }

    public void remove(Long id){
        UserGoingToEvent user = userGoingToEventRepository.getOne(id);
        if (user != null){
            user.setDeleted(true);
            userGoingToEventRepository.save(user);
        }
    }

}
