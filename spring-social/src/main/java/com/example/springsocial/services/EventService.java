package com.example.springsocial.services;

import com.example.springsocial.model.Event;
import com.example.springsocial.repository.EventRepository;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    public List<Event> liveEvents(){
        List<Event> allEvents = eventRepository.findAll();
        Date currentDate = new Date();
        List<Event> retval = new ArrayList<>();
        for (Event e:allEvents) {
            if (currentDate.after(e.getDate()) && currentDate.before(e.getEndTime())){
                retval.add(e);
            }
        }
        return retval;
    }

    public Page<Event> findAll(Pageable pageable){
        Page<Event> events = eventRepository.findAllByOrderByDateOfCreationDesc(pageable);
        return events;
    }

    public Event findById(Long id){
        return eventRepository.getOne(id);
    }

    public Event save(Event event){
        return eventRepository.save(event);
    }

    public void saveFile(byte[] bytes, String path) throws IOException {
        Files.write(Paths.get(path), bytes);
    }

    public Page<Event> search(String text, Pageable pageable){
        return eventRepository.search(text, pageable);
    }

    public void remove(Long id){
        Event event = eventRepository.getOne(id);
        if (event != null){
            event.setDeleted(true);
            eventRepository.save(event);
        }
    }

    public Page<Event> findAllOrderByTitleAsc(Pageable pageable){
        return eventRepository.findAllByOrderByTitleAsc(pageable);
    }

    public Page<Event> findAllOrderByTitleDesc(Pageable pageable){
        return eventRepository.findAllByOrderByTitleDesc(pageable);
    }

    public Page<Event> findAllOrderByDateAsc(Pageable pageable){
        return eventRepository.findAllByOrderByDateAsc(pageable);
    }

    public Page<Event> findAllOrderByDateDesc(Pageable pageable){
        return eventRepository.findAllByOrderByDateDesc(pageable);
    }

    public Page<Event> findAllOrderByConfirmedArrivalsAsc(Pageable pageable){
        return eventRepository.findAllByOrderByNumberOfConfirmedArrivalsAsc(pageable);
    }

    public Page<Event> findAllOrderByConfirmedArrivalsDesc(Pageable pageable){
        return eventRepository.findAllByOrderByNumberOfConfirmedArrivalsDesc(pageable);
    }
}
