package com.example.springsocial.services;

import com.example.springsocial.model.LikeDislike;
import com.example.springsocial.repository.LikeDislikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LikeDislikeService {

    @Autowired
    private LikeDislikeRepository likeDislikeRepository;

    public List<LikeDislike> findAll(){
        return likeDislikeRepository.findAll();
    }

    public LikeDislike findById(Long id){
        return likeDislikeRepository.getOne(id);
    }

    public LikeDislike save(LikeDislike likeDislike){
        return likeDislikeRepository.save(likeDislike);
    }

    public LikeDislike findByEventIdAndUserId(Long eventId, Long userId){
        return likeDislikeRepository.findByEvent_IdAndUser_Id(eventId, userId);
    }

    public void remove(Long id){
        LikeDislike like = likeDislikeRepository.getOne(id);
        if (like != null){
            like.setDeleted(true);
            likeDislikeRepository.save(like);
        }
    }

    public List<LikeDislike> findByEventId(Long id){
        return likeDislikeRepository.findByEvent_Id(id);
    }

    public List<LikeDislike> findByUserId(Long id){
        return likeDislikeRepository.findByUser_Id(id);
    }

}
