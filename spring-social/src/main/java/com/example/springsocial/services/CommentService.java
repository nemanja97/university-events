package com.example.springsocial.services;

import com.example.springsocial.model.Comment;
import com.example.springsocial.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    public Page<Comment> findAll(Pageable pageable){
        Page<Comment> comments = commentRepository.findAll(pageable);
        return comments;
    }

    public Comment findById(Long id){
        return commentRepository.getOne(id);
    }

    public Comment save(Comment comment){
        return commentRepository.save(comment);
    }

    public List<Comment> getByEventId(Long id){
        return commentRepository.findByEvent_IdOrderByDateDesc(id);
    }

    public List<Comment> findByUserId(Long id){
        return commentRepository.findByUser_Id(id);
    }

    public void remove(Long id){
        Comment comment = commentRepository.getOne(id);
        if (comment != null){
            comment.setDeleted(true);
            commentRepository.save(comment);
        }
    }

}
