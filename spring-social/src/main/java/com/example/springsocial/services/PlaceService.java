package com.example.springsocial.services;

import com.example.springsocial.model.Place;
import com.example.springsocial.repository.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaceService {

    @Autowired
    private PlaceRepository placeRepository;

    public List<Place> findAll(){
        return placeRepository.findAll();
    }

    public Place findById(Long id){
        return placeRepository.getOne(id);
    }

    public Place save(Place place){
        return placeRepository.save(place);
    }

}
