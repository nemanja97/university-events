package com.example.springsocial.repository;

import com.example.springsocial.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByEvent_IdOrderByDateDesc(Long id);
    List<Comment> findByUser_Id(Long id);

}
