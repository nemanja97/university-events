package com.example.springsocial.repository;

import com.example.springsocial.model.Event;
import com.example.springsocial.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);
    Boolean existsByEmail(String email);
    User findByUsername(String username);
    User findUserByEmail(String email);

    @Query(value = "select * from users u join place p on u.place_id = p.id where (u.username like %:text% or u.first_name like %:text% or u.last_name like %:text% or u.email like %:text% or p.name like %:text%) and u.deleted = 0", nativeQuery = true)
    Page<User> search(@Param("text") String text, Pageable pageable);

    Page<User> findAllByOrderByUsernameAsc(Pageable pageable);
    Page<User> findAllByOrderByUsernameDesc(Pageable pageable);
    Page<User> findAllByOrderByFirstNameAsc(Pageable pageable);
    Page<User> findAllByOrderByFirstNameDesc(Pageable pageable);
    Page<User> findAllByOrderByLastNameAsc(Pageable pageable);
    Page<User> findAllByOrderByLastNameDesc(Pageable pageable);
    Page<User> findAllByOrderByEmailAsc(Pageable pageable);
    Page<User> findAllByOrderByEmailDesc(Pageable pageable);
}
