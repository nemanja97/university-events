package com.example.springsocial.repository;

import com.example.springsocial.model.LikeDislike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LikeDislikeRepository extends JpaRepository<LikeDislike, Long> {

    LikeDislike findByEvent_IdAndUser_Id(Long eventId, Long userId);
    List<LikeDislike> findByEvent_Id(Long id);
    List<LikeDislike> findByUser_Id(Long id);

}
