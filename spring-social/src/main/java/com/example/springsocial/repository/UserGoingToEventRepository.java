package com.example.springsocial.repository;

import com.example.springsocial.model.UserGoingToEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Repository
public interface UserGoingToEventRepository extends JpaRepository<UserGoingToEvent, Long> {

    List<UserGoingToEvent> findByEvent_Id(Long id);
    UserGoingToEvent findByEvent_IdAndUser_Id(Long eventId, Long userId);
    List<UserGoingToEvent> findByUser_Id(Long id);

    @Query(value = "select * from user_going_to_event ug join users u on ug.user_id = u.id where (u.first_name like %:text% or u.last_name like %:text%) and ug.deleted = 0", nativeQuery = true)
    List<UserGoingToEvent> search(@Param("text") String text);

    @Query(value = "select * from user_going_to_event ug join event e on ug.event_id = e.id join faculty f on e.faculty_id = f.id where (e.title like %:text% or e.date like %:text% or f.name like %:text%) and ug.deleted = 0", nativeQuery = true)
    List<UserGoingToEvent> searchByEvent(@Param("text") String text);

}
