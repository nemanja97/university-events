package com.example.springsocial.repository;

import com.example.springsocial.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    Page<Event> findAllByOrderByDateOfCreationDesc(Pageable pageable);

    @Query(value = "select * from event e join faculty f on e.faculty_id = f.id  where (e.title like %:text% or e.date like %:text% or f.name like %:text%) and e.deleted = 0", nativeQuery = true)
    Page<Event> search(@Param("text") String text, Pageable pageable);

    Page<Event> findAllByOrderByTitleAsc(Pageable pageable);
    Page<Event> findAllByOrderByTitleDesc(Pageable pageable);
    Page<Event> findAllByOrderByDateAsc(Pageable pageable);
    Page<Event> findAllByOrderByDateDesc(Pageable pageable);
    Page<Event> findAllByOrderByNumberOfConfirmedArrivalsAsc(Pageable pageable);
    Page<Event> findAllByOrderByNumberOfConfirmedArrivalsDesc(Pageable pageable);

}
