package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.PlaceDTO;
import com.example.springsocial.model.Place;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PlaceToPlaceDTO implements Converter<Place, PlaceDTO> {

    @Override
    public PlaceDTO convert(Place entity){
        PlaceDTO dto = new PlaceDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setZipCode(entity.getZipCode());

        return dto;
    }

    public List<PlaceDTO> convert(List<Place> entities){
        List<PlaceDTO> dtos = new ArrayList<>();
        for (Place place: entities){
            dtos.add(convert(place));
        }

        return dtos;
    }

}
