package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.FacultyDTO;
import com.example.springsocial.model.Faculty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FacultyToFacultyDTO implements Converter<Faculty, FacultyDTO> {

    @Autowired
    private UniversityToUniversityDTO universityToUniversityDTO;

    @Override
    public FacultyDTO convert(Faculty entity){
        FacultyDTO dto = new FacultyDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
        dto.setUniversityDTO(universityToUniversityDTO.convert(entity.getUniversity()));

        return dto;
    }

    public List<FacultyDTO> convert(List<Faculty> entities){
        List<FacultyDTO> dtos = new ArrayList<>();
        for (Faculty faculty: entities){
            dtos.add(convert(faculty));
        }

        return dtos;
    }

}
