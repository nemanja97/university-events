package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.UserDTO;
import com.example.springsocial.model.AuthProvider;
import com.example.springsocial.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Component
public class UserToUserDTO implements Converter<User, UserDTO> {

    @Autowired
    private PlaceToPlaceDTO placeToPlaceDTO;

    @Override
    public UserDTO convert(User entity){
        UserDTO dto = new UserDTO();
        dto.setId(entity.getId());
        dto.setUsername(entity.getUsername());
        dto.setPassword("");
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setEmail(entity.getEmail());
//        dto.setImageUrl(entity.getImageUrl());
        dto.setEmailVerified(entity.getEmailVerified());
        dto.setProvider(entity.getProvider());
        dto.setRole(entity.getRole());
        dto.setDeleted(entity.isDeleted());
        dto.setProviderId(entity.getProviderId());
        dto.setBlocked(entity.isBlocked());
        dto.setPlaceDTO(placeToPlaceDTO.convert(entity.getPlace()));

        if(entity.getProvider() == AuthProvider.local){
            if(entity.getImageUrl() != null && !entity.getImageUrl().equals("")){
                File file=new File(entity.getImageUrl());
                String encodedBase64=null;
                try {
                    if(file!= null){
                        FileInputStream fileInputStream=new FileInputStream(file);
                        byte[] bytes=new byte[(int)file.length()];
                        fileInputStream.read(bytes);
                        encodedBase64= Base64.getEncoder().encodeToString(bytes);
                        dto.setImageUrl("data:image/png;base64,"+encodedBase64);
                        fileInputStream.close();
                    }
                    else dto.setImageUrl(entity.getImageUrl());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {dto.setImageUrl(entity.getImageUrl());}
        }else{
            dto.setImageUrl(entity.getImageUrl());
        }

        return dto;

    }

    public List<UserDTO> convert(List<User> entities){
        List<UserDTO> dtos = new ArrayList<>();
        for (User user: entities){
            dtos.add(convert(user));
        }

        return dtos;
    }

}
