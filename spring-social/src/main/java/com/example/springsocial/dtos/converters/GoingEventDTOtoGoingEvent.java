package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.UserGoingToEventDTO;
import com.example.springsocial.model.Event;
import com.example.springsocial.model.User;
import com.example.springsocial.model.UserGoingToEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GoingEventDTOtoGoingEvent implements Converter<UserGoingToEventDTO, UserGoingToEvent> {

    @Autowired
    private EventDTOtoEvent eventDTOtoEvent;

    @Autowired
    private UserDTOtoUser userDTOtoUser;

    @Override
    public UserGoingToEvent convert(UserGoingToEventDTO entity){
        UserGoingToEvent userGoingToEvent = new UserGoingToEvent();
        userGoingToEvent.setId(entity.getId());
        userGoingToEvent.setDeleted(entity.isDeleted());
        Event event = eventDTOtoEvent.convert(entity.getEventDTO());
        if (event != null)
            userGoingToEvent.setEvent(event);
        User user = userDTOtoUser.convert(entity.getUserDTO());
        if (user != null)
            userGoingToEvent.setUser(user);

        return userGoingToEvent;
    }

    public List<UserGoingToEvent> convert(List<UserGoingToEventDTO> entities){
        List<UserGoingToEvent> userGoingToEvents = new ArrayList<>();
        for (UserGoingToEventDTO dto: entities){
            userGoingToEvents.add(convert(dto));
        }

        return userGoingToEvents;
    }

}
