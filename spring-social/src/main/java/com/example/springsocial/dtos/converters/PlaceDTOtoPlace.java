package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.PlaceDTO;
import com.example.springsocial.model.Place;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PlaceDTOtoPlace implements Converter<PlaceDTO, Place> {

    @Override
    public Place convert(PlaceDTO entity){
        Place place = new Place();
        place.setId(entity.getId());
        place.setName(entity.getName());
        place.setZipCode(entity.getZipCode());

        return place;
    }

    public List<Place> convert(List<PlaceDTO> entities){
        List<Place> places = new ArrayList<>();
        for (PlaceDTO dto: entities){
            places.add(convert(dto));
        }

        return places;
    }

}
