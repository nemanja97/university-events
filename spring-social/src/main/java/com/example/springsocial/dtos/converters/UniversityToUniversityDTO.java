package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.UniversityDTO;
import com.example.springsocial.model.University;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UniversityToUniversityDTO implements Converter<University, UniversityDTO> {

    @Autowired
    private PlaceToPlaceDTO placeToPlaceDTO;

    @Override
    public UniversityDTO convert(University entity){
        UniversityDTO dto = new UniversityDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
        dto.setEmail(entity.getEmail());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setPlaceDTO(placeToPlaceDTO.convert(entity.getPlace()));

        return dto;
    }

    public List<UniversityDTO> convert(List<University> entities){
        List<UniversityDTO> dtos = new ArrayList<>();
        for (University university: entities){
            dtos.add(convert(university));
        }

        return dtos;
    }

}
