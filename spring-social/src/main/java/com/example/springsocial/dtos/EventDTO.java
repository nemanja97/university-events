package com.example.springsocial.dtos;

import com.example.springsocial.model.Event;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class EventDTO implements Serializable {

    private Long id;
    private String title;
    private String description;
    //    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private Date date;
    //    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private Date endTime;
    private String coverPhoto;
    private int numberOfConfirmedArrivals;
    private String instructor;
    private int numberOfLike;
    private int numberOfDislike;
    private Date dateOfCreation;
    private boolean deleted;
    private boolean isLive;
    private Date liveDuration;
    private FacultyDTO facultyDTO;

    public EventDTO(){

    }

    public EventDTO(Long id, String title, String description, Date date, Date endTime, String coverPhoto, int numberOfConfirmedArrivals, String instructor, int numberOfLike, int numberOfDislike, Date dateOfCreation, boolean deleted, boolean isLive, Date liveDuration, FacultyDTO facultyDTO) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.endTime = endTime;
        this.coverPhoto = coverPhoto;
        this.numberOfConfirmedArrivals = numberOfConfirmedArrivals;
        this.instructor = instructor;
        this.numberOfLike = numberOfLike;
        this.numberOfDislike = numberOfDislike;
        this.dateOfCreation = dateOfCreation;
        this.deleted = deleted;
        this.isLive = isLive;
        this.liveDuration = liveDuration;
        this.facultyDTO = facultyDTO;
    }

    public EventDTO(Event event){
        this(event.getId(),
                event.getTitle(),
                event.getDescription(),
                event.getDate(),
                event.getEndTime(),
                event.getCoverPhoto(),
                event.getNumberOfConfirmedArrivals(),
                event.getInstructor(),
                event.getNumberOfLike(),
                event.getNumberOfDislike(),
                event.getDateOfCreation(),
                event.isDeleted(),
                event.isLive(),
                event.getLiveDuration(),
                new FacultyDTO(event.getFaculty()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public int getNumberOfConfirmedArrivals() {
        return numberOfConfirmedArrivals;
    }

    public void setNumberOfConfirmedArrivals(int numberOfConfirmedArrivals) {
        this.numberOfConfirmedArrivals = numberOfConfirmedArrivals;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public FacultyDTO getFacultyDTO() {
        return facultyDTO;
    }

    public void setFacultyDTO(FacultyDTO facultyDTO) {
        this.facultyDTO = facultyDTO;
    }

    public int getNumberOfLike() {
        return numberOfLike;
    }

    public void setNumberOfLike(int numberOfLike) {
        this.numberOfLike = numberOfLike;
    }

    public int getNumberOfDislike() {
        return numberOfDislike;
    }

    public void setNumberOfDislike(int numberOfDislike) {
        this.numberOfDislike = numberOfDislike;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public boolean isLive() {
        return isLive;
    }

    public void setLive(boolean live) {
        isLive = live;
    }

    public Date getLiveDuration() {
        return liveDuration;
    }

    public void setLiveDuration(Date liveDuration) {
        this.liveDuration = liveDuration;
    }
}
