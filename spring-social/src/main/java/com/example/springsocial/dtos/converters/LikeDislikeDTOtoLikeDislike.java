package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.LikeDislikeDTO;
import com.example.springsocial.model.Event;
import com.example.springsocial.model.LikeDislike;
import com.example.springsocial.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LikeDislikeDTOtoLikeDislike implements Converter<LikeDislikeDTO, LikeDislike> {

    @Autowired
    private UserDTOtoUser userDTOtoUser;

    @Autowired
    private EventDTOtoEvent eventDTOtoEvent;

    @Override
    public LikeDislike convert(LikeDislikeDTO entity){
        LikeDislike likeDislike = new LikeDislike();
        likeDislike.setId(entity.getId());
        likeDislike.setLiked(entity.isLiked());
        likeDislike.setDeleted(entity.isDeleted());
        User user = userDTOtoUser.convert(entity.getUserDTO());
        if (user != null)
            likeDislike.setUser(user);
        if (entity.getEventDTO() != null) {
            Event event = eventDTOtoEvent.convert(entity.getEventDTO());
            likeDislike.setEvent(event);
        }

        return likeDislike;
    }

    public List<LikeDislike> convert(List<LikeDislikeDTO> entities){
        List<LikeDislike> likeDislikes = new ArrayList<>();
        for (LikeDislikeDTO dto: entities){
            likeDislikes.add(convert(dto));
        }

        return likeDislikes;
    }

}
