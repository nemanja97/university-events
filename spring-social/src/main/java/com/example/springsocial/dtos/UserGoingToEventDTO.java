package com.example.springsocial.dtos;

import com.example.springsocial.model.UserGoingToEvent;

import java.io.Serializable;

public class UserGoingToEventDTO implements Serializable {

    private Long id;
    private boolean deleted;
    private EventDTO eventDTO;
    private UserDTO userDTO;

    public UserGoingToEventDTO(){

    }

    public UserGoingToEventDTO(Long id, boolean deleted, EventDTO eventDTO, UserDTO userDTO) {
        this.id = id;
        this.deleted = deleted;
        this.eventDTO = eventDTO;
        this.userDTO = userDTO;
    }

    public UserGoingToEventDTO(UserGoingToEvent userGoingToEvent){
        this(userGoingToEvent.getId(),
                userGoingToEvent.isDeleted(),
                new EventDTO(userGoingToEvent.getEvent()),
                new UserDTO(userGoingToEvent.getUser()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EventDTO getEventDTO() {
        return eventDTO;
    }

    public void setEventDTO(EventDTO eventDTO) {
        this.eventDTO = eventDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}

