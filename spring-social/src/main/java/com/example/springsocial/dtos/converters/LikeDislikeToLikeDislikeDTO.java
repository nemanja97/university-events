package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.LikeDislikeDTO;
import com.example.springsocial.model.LikeDislike;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LikeDislikeToLikeDislikeDTO implements Converter<LikeDislike, LikeDislikeDTO> {

    @Autowired
    private UserToUserDTO userToUserDTO;

    @Autowired
    private EventToEventDTO eventToEventDTO;

    @Override
    public LikeDislikeDTO convert(LikeDislike entity){
        LikeDislikeDTO dto = new LikeDislikeDTO();
        dto.setId(entity.getId());
        dto.setLiked(entity.isLiked());
        dto.setDeleted(entity.isDeleted());
        if (entity.getUser() != null)
            dto.setUserDTO(userToUserDTO.convert(entity.getUser()));
        if (entity.getEvent() != null)
            dto.setEventDTO(eventToEventDTO.convert(entity.getEvent()));

        return dto;
    }

    public List<LikeDislikeDTO> convert(List<LikeDislike> entities){
        List<LikeDislikeDTO> dtos = new ArrayList<>();
        for (LikeDislike likeDislike: entities){
            dtos.add(convert(likeDislike));
        }

        return dtos;
    }

}
