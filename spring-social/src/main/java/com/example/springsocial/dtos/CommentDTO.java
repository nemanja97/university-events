package com.example.springsocial.dtos;

import com.example.springsocial.model.Comment;

import java.io.Serializable;
import java.util.Date;

public class CommentDTO implements Serializable {

    private Long id;
    private String content;
    private Date date;
    private boolean deleted;
    private EventDTO eventDTO;
    private UserDTO userDTO;

    public CommentDTO(){

    }

    public CommentDTO(Long id, String content, Date date, boolean deleted, EventDTO eventDTO, UserDTO userDTO) {
        this.id = id;
        this.content = content;
        this.date = date;
        this.deleted = deleted;
        this.eventDTO = eventDTO;
        this.userDTO = userDTO;
    }

    public CommentDTO(Comment comment){
        this(comment.getId(),
                comment.getContent(),
                comment.getDate(),
                comment.isDeleted(),
                new EventDTO(comment.getEvent()),
                new UserDTO(comment.getUser()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public EventDTO getEventDTO() {
        return eventDTO;
    }

    public void setEventDTO(EventDTO eventDTO) {
        this.eventDTO = eventDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
