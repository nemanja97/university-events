package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.FacultyDTO;
import com.example.springsocial.model.Faculty;
import com.example.springsocial.model.University;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FacultyDTOtoFaculty implements Converter<FacultyDTO, Faculty> {

    @Autowired
    private UniversityDTOtoUniversity universityDTOtoUniversity;

    @Override
    public Faculty convert(FacultyDTO entity){
        Faculty faculty = new Faculty();
        faculty.setId(entity.getId());
        faculty.setName(entity.getName());
        faculty.setAddress(entity.getAddress());
        University university = universityDTOtoUniversity.convert(entity.getUniversityDTO());
        if (university != null)
            faculty.setUniversity(university);

        return faculty;
    }

    public List<Faculty> convert(List<FacultyDTO> entities){
        List<Faculty> faculties = new ArrayList<>();
        for (FacultyDTO dto: entities){
            faculties.add(convert(dto));
        }

        return faculties;
    }

}
