package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.CommentDTO;
import com.example.springsocial.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CommentToCommentDTO implements Converter<Comment, CommentDTO> {

    @Autowired
    private EventToEventDTO eventToEventDTO;

    @Autowired
    private UserToUserDTO userToUserDTO;

    @Override
    public CommentDTO convert(Comment entity){
        CommentDTO dto = new CommentDTO();
        dto.setId(entity.getId());
        dto.setContent(entity.getContent());
        dto.setDate(entity.getDate());
        dto.setDeleted(entity.isDeleted());
        dto.setEventDTO(eventToEventDTO.convert(entity.getEvent()));
        dto.setUserDTO(userToUserDTO.convert(entity.getUser()));

        return dto;
    }

    public List<CommentDTO> convert(List<Comment> entities){
        List<CommentDTO> dtos = new ArrayList<>();
        for (Comment comment: entities){
            dtos.add(convert(comment));
        }

        return dtos;
    }

}
