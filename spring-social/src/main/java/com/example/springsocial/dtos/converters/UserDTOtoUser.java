package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.UserDTO;
import com.example.springsocial.model.Place;
import com.example.springsocial.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserDTOtoUser implements Converter<UserDTO, User> {


    @Autowired
    private PlaceDTOtoPlace placeDTOtoPlace;

    @Override
    public User convert(UserDTO entity){
        User user = new User();
        user.setId(entity.getId());
        user.setUsername(entity.getUsername());
        user.setPassword(entity.getPassword());
        user.setFirstName(entity.getFirstName());
        user.setLastName(entity.getLastName());
        user.setEmail(entity.getEmail());
        user.setImageUrl(entity.getImageUrl());
        user.setEmailVerified(entity.getEmailVerified());
        user.setProvider(entity.getProvider());
        user.setRole(entity.getRole());
        user.setDeleted(entity.isDeleted());
        user.setProviderId(entity.getProviderId());
        user.setBlocked(entity.isBlocked());
        Place place = placeDTOtoPlace.convert(entity.getPlaceDTO());
        if (place != null)
            user.setPlace(place);



        return user;
    }

    public List<User> convert(List<UserDTO> entities){
        List<User> users = new ArrayList<>();
        for (UserDTO dto: entities){
            users.add(convert(dto));
        }

        return users;
    }

}
