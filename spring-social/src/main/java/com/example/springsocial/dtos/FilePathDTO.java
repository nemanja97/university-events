package com.example.springsocial.dtos;

public class FilePathDTO {

    private String path;

    public FilePathDTO(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
