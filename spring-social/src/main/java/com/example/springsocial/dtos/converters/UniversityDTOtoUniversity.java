package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.UniversityDTO;
import com.example.springsocial.model.Place;
import com.example.springsocial.model.University;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UniversityDTOtoUniversity implements Converter<UniversityDTO, University>{

    @Autowired
    private PlaceDTOtoPlace placeDTOtoPlace;

    @Override
    public University convert(UniversityDTO entity){
        University university = new University();
        university.setId(entity.getId());
        university.setName(entity.getName());
        university.setAddress(entity.getAddress());
        university.setEmail(entity.getEmail());
        university.setPhoneNumber(entity.getPhoneNumber());
        Place place = placeDTOtoPlace.convert(entity.getPlaceDTO());
        if (place != null)
            university.setPlace(place);

        return university;
    }

    public List<University> convert(List<UniversityDTO> entities){
        List<University> universities = new ArrayList<>();
        for (UniversityDTO dto: entities){
            universities.add(convert(dto));
        }

        return universities;
    }

}
