package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.UserGoingToEventDTO;
import com.example.springsocial.model.UserGoingToEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GoingEventToGoingEventDTO implements Converter<UserGoingToEvent, UserGoingToEventDTO> {

    @Autowired
    private EventToEventDTO eventToEventDTO;

    @Autowired
    private UserToUserDTO userToUserDTO;

    @Override
    public UserGoingToEventDTO convert(UserGoingToEvent entity){
        UserGoingToEventDTO dto = new UserGoingToEventDTO();
        dto.setId(entity.getId());
        dto.setDeleted(entity.isDeleted());
        if (entity.getEvent() != null)
            dto.setEventDTO(eventToEventDTO.convert(entity.getEvent()));
        if(entity.getUser() != null)
            dto.setUserDTO(userToUserDTO.convert(entity.getUser()));

        return dto;
    }

    public List<UserGoingToEventDTO> convert(List<UserGoingToEvent> entities){
        List<UserGoingToEventDTO> dtos = new ArrayList<>();
        for (UserGoingToEvent userGoingToEvent: entities){
            dtos.add(convert(userGoingToEvent));
        }

        return dtos;
    }

}
