package com.example.springsocial.dtos;

import com.example.springsocial.model.Place;

import java.io.Serializable;

public class PlaceDTO implements Serializable {

    private Long id;
    private String name;
    private int zipCode;

    public PlaceDTO(){

    }

    public PlaceDTO(Long id, String name, int zipCode) {
        this.id = id;
        this.name = name;
        this.zipCode = zipCode;
    }

    public PlaceDTO(Place place){
        this(place.getId(),
                place.getName(),
                place.getZipCode());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }
}
