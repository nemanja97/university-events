package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.EventDTO;
import com.example.springsocial.model.Event;
import com.example.springsocial.model.Faculty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EventDTOtoEvent implements Converter<EventDTO, Event> {

    @Autowired
    private FacultyDTOtoFaculty facultyDTOtoFaculty;

    @Override
    public Event convert(EventDTO entity){
        Event event = new Event();
        event.setId(entity.getId());
        event.setTitle(entity.getTitle());
        event.setDescription(entity.getDescription());
        event.setDate(entity.getDate());
        event.setEndTime(entity.getEndTime());
        event.setCoverPhoto(entity.getCoverPhoto());
        event.setNumberOfConfirmedArrivals(entity.getNumberOfConfirmedArrivals());
        event.setInstructor(entity.getInstructor());
        event.setNumberOfLike(entity.getNumberOfLike());
        event.setNumberOfDislike(entity.getNumberOfDislike());
        event.setDateOfCreation(entity.getDateOfCreation());
        event.setDeleted(entity.isDeleted());
        event.setLive(entity.isLive());
        event.setLiveDuration(entity.getLiveDuration());
        Faculty faculty = facultyDTOtoFaculty.convert(entity.getFacultyDTO());
        if (faculty != null)
            event.setFaculty(faculty);

        return event;
    }

    public List<Event> convert(List<EventDTO> entities){
        List<Event> events = new ArrayList<>();
        for (EventDTO dto: entities){
            events.add(convert(dto));
        }

        return events;
    }

}
