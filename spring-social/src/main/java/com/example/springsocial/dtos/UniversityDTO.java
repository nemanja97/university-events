package com.example.springsocial.dtos;

import com.example.springsocial.model.University;

import java.io.Serializable;

public class UniversityDTO implements Serializable {

    private Long id;
    private String name;
    private String address;
    private String email;
    private String phoneNumber;
    private PlaceDTO placeDTO;

    public UniversityDTO(){

    }

    public UniversityDTO(Long id, String name, String address, String email, String phoneNumber, PlaceDTO placeDTO) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.placeDTO = placeDTO;
    }

    public UniversityDTO(University university){
        this(university.getId(),
                university.getName(),
                university.getAddress(),
                university.getEmail(),
                university.getPhoneNumber(),
                new PlaceDTO(university.getPlace()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public PlaceDTO getPlaceDTO() {
        return placeDTO;
    }

    public void setPlaceDTO(PlaceDTO placeDTO) {
        this.placeDTO = placeDTO;
    }
}
