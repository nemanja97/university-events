package com.example.springsocial.dtos;

public class PasswordDTO {

    private String password;
    private UserDTO userDTO;

    public PasswordDTO(){

    }

    public PasswordDTO(String password, UserDTO userDTO) {
        this.password = password;
        this.userDTO = userDTO;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

}
