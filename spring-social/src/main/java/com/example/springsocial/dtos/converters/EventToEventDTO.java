package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.EventDTO;
import com.example.springsocial.model.Event;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Component
public class EventToEventDTO implements Converter<Event, EventDTO> {

    @Autowired
    private FacultyToFacultyDTO facultyToFacultyDTO;

    @Override
    public EventDTO convert(Event entity){
        EventDTO dto = new EventDTO();
        dto.setId(entity.getId());
        dto.setTitle(entity.getTitle());
        dto.setDescription(entity.getDescription());
        dto.setDate(entity.getDate());
        dto.setEndTime(entity.getEndTime());
        dto.setNumberOfConfirmedArrivals(entity.getNumberOfConfirmedArrivals());
        dto.setInstructor(entity.getInstructor());
        dto.setNumberOfLike(entity.getNumberOfLike());
        dto.setNumberOfDislike(entity.getNumberOfDislike());
        dto.setDateOfCreation(entity.getDateOfCreation());
        dto.setDeleted(entity.isDeleted());
        dto.setLive(entity.isLive());
        dto.setLiveDuration(entity.getLiveDuration());
        dto.setFacultyDTO(facultyToFacultyDTO.convert(entity.getFaculty()));

        if(entity.getCoverPhoto() != null && !entity.getCoverPhoto().equals("")){
            File file=new File(entity.getCoverPhoto());
            String encodedBase64=null;
            try {
                if(file!= null){
                    FileInputStream fileInputStream=new FileInputStream(file);
                    byte[] bytes=new byte[(int)file.length()];
                    fileInputStream.read(bytes);
                    encodedBase64= Base64.getEncoder().encodeToString(bytes);
                    dto.setCoverPhoto("data:image/jpg;base64,"+encodedBase64);
                    fileInputStream.close();
                }
                else dto.setCoverPhoto(entity.getCoverPhoto());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {dto.setCoverPhoto(entity.getCoverPhoto());}

        return dto;
    }

    public List<EventDTO> convert(List<Event> entities){
        List<EventDTO> dtos = new ArrayList<>();
        for (Event event: entities){
            dtos.add(convert(event));
        }

        return dtos;
    }

}
