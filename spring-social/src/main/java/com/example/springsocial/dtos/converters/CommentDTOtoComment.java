package com.example.springsocial.dtos.converters;

import com.example.springsocial.dtos.CommentDTO;
import com.example.springsocial.model.Comment;
import com.example.springsocial.model.Event;
import com.example.springsocial.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CommentDTOtoComment implements Converter<CommentDTO, Comment> {

    @Autowired
    private EventDTOtoEvent eventDTOtoEvent;

    @Autowired
    private UserDTOtoUser userDTOtoUser;

    @Override
    public Comment convert(CommentDTO entity){
        Comment comment = new Comment();
        comment.setId(entity.getId());
        comment.setContent(entity.getContent());
        comment.setDate(entity.getDate());
        comment.setDeleted(entity.isDeleted());
        Event event = eventDTOtoEvent.convert(entity.getEventDTO());
        if (event != null)
            comment.setEvent(event);
        User user = userDTOtoUser.convert(entity.getUserDTO());
        if (user != null)
            comment.setUser(user);

        return comment;
    }

    public List<Comment> convert(List<CommentDTO> entities){
        List<Comment> comments = new ArrayList<>();
        for (CommentDTO dto: entities){
            comments.add(convert(dto));
        }

        return comments;
    }

}