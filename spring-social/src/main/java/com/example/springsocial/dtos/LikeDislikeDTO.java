package com.example.springsocial.dtos;

import com.example.springsocial.model.LikeDislike;

import java.io.Serializable;

public class LikeDislikeDTO implements Serializable {

    private Long id;
    private boolean isLiked;
    private boolean deleted;
    private UserDTO userDTO;
    private EventDTO eventDTO;

    public LikeDislikeDTO(){

    }

    public LikeDislikeDTO(Long id, boolean isLiked, boolean deleted, UserDTO userDTO, EventDTO eventDTO) {
        this.id = id;
        this.isLiked = isLiked;
        this.deleted = deleted;
        this.userDTO = userDTO;
        this.eventDTO = eventDTO;
    }

    public LikeDislikeDTO(LikeDislike likeDislike){
        this(likeDislike.getId(),
                likeDislike.isLiked(),
                likeDislike.isDeleted(),
                new UserDTO(likeDislike.getUser()),
                new EventDTO(likeDislike.getEvent()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public EventDTO getEventDTO() {
        return eventDTO;
    }

    public void setEventDTO(EventDTO eventDTO) {
        this.eventDTO = eventDTO;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
