package com.example.springsocial.dtos;

import com.example.springsocial.model.Faculty;

import java.io.Serializable;

public class FacultyDTO implements Serializable {

    private Long id;
    private String name;
    private String address;
    private UniversityDTO universityDTO;

    public FacultyDTO(){

    }

    public FacultyDTO(Long id, String name, String address, UniversityDTO universityDTO) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.universityDTO = universityDTO;
    }

    public FacultyDTO(Faculty faculty){
        this(faculty.getId(),
                faculty.getName(),
                faculty.getAddress(),
                new UniversityDTO(faculty.getUniversity()));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UniversityDTO getUniversityDTO() {
        return universityDTO;
    }

    public void setUniversityDTO(UniversityDTO universityDTO) {
        this.universityDTO = universityDTO;
    }
}
