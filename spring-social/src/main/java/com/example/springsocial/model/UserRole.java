package com.example.springsocial.model;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN
}
