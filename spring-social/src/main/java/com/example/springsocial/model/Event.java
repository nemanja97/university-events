package com.example.springsocial.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Where(clause = "deleted = false")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, length = 70)
    private String title;

    @Column(name = "description", nullable = false, length = 220)
    private String description;

    @Column(name = "date", nullable = false)
    private Date date;

    @Column(name = "endTime", nullable = false)
    private Date endTime;

    @Column(name = "coverPhoto", nullable = false)
    private String coverPhoto;

    @Column(name = "numberOfConfirmedArrivals", nullable = false)
    private int numberOfConfirmedArrivals;

    @Column(name = "instructor", nullable = false, length = 40)
    private String instructor;

    @Column(name = "numberOfLike", nullable = false)
    private int numberOfLike;

    @Column(name = "numberOfDislike", nullable = false)
    private int numberOfDislike;

    @Column(name = "dateOfCreation", nullable = false)
    private Date dateOfCreation;

    @Column(name = "deleted", nullable = false)
    private boolean deleted;

    @Column(name = "isLive", nullable = false)
    private boolean isLive;

    @Column(name = "liveDuration")
    private Date liveDuration;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "event")
    private Set<Comment> comments = new HashSet<Comment>();

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "event")
    private Set<UserGoingToEvent> userGoingToEvents = new HashSet<UserGoingToEvent>();

    @ManyToOne
    @JoinColumn(name = "faculty_id", referencedColumnName = "id", nullable = false)
    private Faculty faculty;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "event")
    private Set<LikeDislike> likesDislikes = new HashSet<LikeDislike>();

    public Event(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public int getNumberOfConfirmedArrivals() {
        return numberOfConfirmedArrivals;
    }

    public void setNumberOfConfirmedArrivals(int numberOfConfirmedArrivals) {
        this.numberOfConfirmedArrivals = numberOfConfirmedArrivals;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public int getNumberOfLike() {
        return numberOfLike;
    }

    public void setNumberOfLike(int numberOfLike) {
        this.numberOfLike = numberOfLike;
    }

    public int getNumberOfDislike() {
        return numberOfDislike;
    }

    public void setNumberOfDislike(int numberOfDislike) {
        this.numberOfDislike = numberOfDislike;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<UserGoingToEvent> getUserGoingToEvents() {
        return userGoingToEvents;
    }

    public void setUserGoingToEvents(Set<UserGoingToEvent> userGoingToEvents) {
        this.userGoingToEvents = userGoingToEvents;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public Set<LikeDislike> getLikesDislikes() {
        return likesDislikes;
    }

    public void setLikesDislikes(Set<LikeDislike> likesDislikes) {
        this.likesDislikes = likesDislikes;
    }

    public boolean isLive() {
        return isLive;
    }

    public void setLive(boolean live) {
        isLive = live;
    }

    public Date getLiveDuration() {
        return liveDuration;
    }

    public void setLiveDuration(Date liveDuration) {
        this.liveDuration = liveDuration;
    }
}
