package com.example.springsocial.controller;

import com.example.springsocial.dtos.LikeDislikeDTO;
import com.example.springsocial.dtos.converters.LikeDislikeDTOtoLikeDislike;
import com.example.springsocial.dtos.converters.LikeDislikeToLikeDislikeDTO;
import com.example.springsocial.model.Event;
import com.example.springsocial.model.LikeDislike;
import com.example.springsocial.model.User;
import com.example.springsocial.services.EventService;
import com.example.springsocial.services.LikeDislikeService;
import com.example.springsocial.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping(value = "api/likes")
public class LikeDislikeController {

    @Autowired
    private LikeDislikeService likeDislikeService;

    @Autowired
    private LikeDislikeToLikeDislikeDTO likeDislikeToLikeDislikeDTO;

    @Autowired
    private LikeDislikeDTOtoLikeDislike likeDislikeDTOtoLikeDislike;

    @Autowired
    private EventService eventService;

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<LikeDislikeDTO>> getAll(){
        List<LikeDislike> likeDislikes = likeDislikeService.findAll();

        return new ResponseEntity<>(likeDislikeToLikeDislikeDTO.convert(likeDislikes), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<LikeDislikeDTO> getOne(@PathVariable("id") Long id){
        LikeDislike likeDislike = likeDislikeService.findById(id);
        if (likeDislike == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(likeDislikeToLikeDislikeDTO.convert(likeDislike), HttpStatus.CREATED);
    }

    @GetMapping(value = "/userLike/{eventId}/{userId}")
    public ResponseEntity<LikeDislikeDTO> findBrEventIdAndUserId(@PathVariable("eventId") Long eventId, @PathVariable("userId") Long userId){
        if (eventId == null || userId == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        LikeDislike likeDislike = likeDislikeService.findByEventIdAndUserId(eventId, userId);
        if (likeDislike == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(likeDislikeToLikeDislikeDTO.convert(likeDislike), HttpStatus.OK);
    }

    @PostMapping(value = "/like")
    public ResponseEntity<LikeDislikeDTO> create(@RequestBody LikeDislikeDTO likeDislikeDTO){
        LikeDislike likeDislike = likeDislikeDTOtoLikeDislike.convert(likeDislikeDTO);

        Event foundEvent = eventService.findById(likeDislikeDTO.getEventDTO().getId());
        likeDislike.setLiked(true);
        likeDislike = likeDislikeService.save(likeDislike);

        User user = userService.findById(likeDislike.getUser().getId());
        user.setImageUrl(user.getImageUrl());
        user = userService.save(user);

        foundEvent.setCoverPhoto(foundEvent.getCoverPhoto());
        foundEvent.setNumberOfLike(foundEvent.getNumberOfLike()+1);
        foundEvent = eventService.save(foundEvent);

        likeDislike.setEvent(foundEvent);
        likeDislike.setUser(user);

        return new ResponseEntity<>(likeDislikeToLikeDislikeDTO.convert(likeDislike), HttpStatus.CREATED);
    }

    @PostMapping(value = "/dislike")
    public ResponseEntity<LikeDislikeDTO> dislike(@RequestBody LikeDislikeDTO likeDislikeDTO){
        LikeDislike likeDislike = likeDislikeDTOtoLikeDislike.convert(likeDislikeDTO);

        Event foundEvent = eventService.findById(likeDislikeDTO.getEventDTO().getId());
        likeDislike.setLiked(false);
        likeDislike = likeDislikeService.save(likeDislike);

        User user = userService.findById(likeDislike.getUser().getId());
        user.setImageUrl(user.getImageUrl());
        user = userService.save(user);

        foundEvent.setCoverPhoto(foundEvent.getCoverPhoto());
        foundEvent.setNumberOfDislike(foundEvent.getNumberOfDislike()+1);
        foundEvent = eventService.save(foundEvent);

        likeDislike.setEvent(foundEvent);
        likeDislike.setUser(user);

        return new ResponseEntity<>(likeDislikeToLikeDislikeDTO.convert(likeDislike), HttpStatus.CREATED);
    }

    @PutMapping(value = "/changeLike/{id}")
    public ResponseEntity<LikeDislikeDTO> changeLike(@RequestBody LikeDislikeDTO likeDislikeDTO, @PathVariable("id") Long id){
        LikeDislike likeDislike = likeDislikeDTOtoLikeDislike.convert(likeDislikeDTO);

        Event foundEvent = eventService.findById(likeDislikeDTO.getEventDTO().getId());

        likeDislike.setLiked(true);
        likeDislike = likeDislikeService.save(likeDislike);
        foundEvent.setNumberOfLike(foundEvent.getNumberOfLike()+1);
        foundEvent.setNumberOfDislike(foundEvent.getNumberOfDislike()-1);
        foundEvent = eventService.save(foundEvent);
        likeDislike.setEvent(foundEvent);

        return new ResponseEntity<>(likeDislikeToLikeDislikeDTO.convert(likeDislike), HttpStatus.OK);
    }

    @PutMapping(value = "/changeDislike/{id}")
    public ResponseEntity<LikeDislikeDTO> changeDislike(@RequestBody LikeDislikeDTO likeDislikeDTO, @PathVariable("id") Long id){
        LikeDislike likeDislike = likeDislikeDTOtoLikeDislike.convert(likeDislikeDTO);

        Event foundEvent = eventService.findById(likeDislikeDTO.getEventDTO().getId());

        likeDislike.setLiked(false);
        likeDislike = likeDislikeService.save(likeDislike);
        foundEvent.setNumberOfDislike(foundEvent.getNumberOfDislike()+1);
        foundEvent.setNumberOfLike(foundEvent.getNumberOfLike()-1);
        foundEvent = eventService.save(foundEvent);
        likeDislike.setEvent(foundEvent);

        return new ResponseEntity<>(likeDislikeToLikeDislikeDTO.convert(likeDislike), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<LikeDislikeDTO> update(@RequestBody LikeDislikeDTO likeDislikeDTO, @PathVariable("id") Long id){
        if (likeDislikeService.findById(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        LikeDislike likeDislike = likeDislikeService.save(likeDislikeDTOtoLikeDislike.convert(likeDislikeDTO));

        return new ResponseEntity<>(likeDislikeToLikeDislikeDTO.convert(likeDislike), HttpStatus.OK);
    }

}
