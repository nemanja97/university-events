package com.example.springsocial.controller;

import com.example.springsocial.dtos.FacultyDTO;
import com.example.springsocial.dtos.converters.FacultyDTOtoFaculty;
import com.example.springsocial.dtos.converters.FacultyToFacultyDTO;
import com.example.springsocial.model.Faculty;
import com.example.springsocial.services.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/faculties")
public class FacultyController {

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private FacultyToFacultyDTO facultyToFacultyDTO;

    @Autowired
    private FacultyDTOtoFaculty facultyDTOtoFaculty;

    @GetMapping
    public ResponseEntity<List<FacultyDTO>> getAll(){
        List<Faculty> faculties = facultyService.findAll();

        return new ResponseEntity<>(facultyToFacultyDTO.convert(faculties), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<FacultyDTO> getOne(@PathVariable("id") Long id){
        Faculty faculty = facultyService.findOne(id);
        if (faculty == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(facultyToFacultyDTO.convert(faculty), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<FacultyDTO> create(@RequestBody FacultyDTO facultyDTO){
        Faculty faculty = facultyService.save(facultyDTOtoFaculty.convert(facultyDTO));

        return new ResponseEntity<>(facultyToFacultyDTO.convert(faculty), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<FacultyDTO> update(@RequestBody FacultyDTO facultyDTO, @PathVariable("id") Long id){
        if (facultyService.findOne(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Faculty faculty = facultyService.save(facultyDTOtoFaculty.convert(facultyDTO));

        return new ResponseEntity<>(facultyToFacultyDTO.convert(faculty), HttpStatus.OK);
    }

}
