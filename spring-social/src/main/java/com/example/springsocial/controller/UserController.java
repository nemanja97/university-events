package com.example.springsocial.controller;

import com.example.springsocial.dtos.UserDTO;
import com.example.springsocial.dtos.converters.UserToUserDTO;
import com.example.springsocial.exception.ResourceNotFoundException;
import com.example.springsocial.model.User;
import com.example.springsocial.repository.UserRepository;
import com.example.springsocial.security.CurrentUser;
import com.example.springsocial.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserToUserDTO userToUserDTO;

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<UserDTO> getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {

        User user = userRepository.getOne(userPrincipal.getId());
        if(user == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<UserDTO>(userToUserDTO.convert(user),HttpStatus.OK);
    }


}
