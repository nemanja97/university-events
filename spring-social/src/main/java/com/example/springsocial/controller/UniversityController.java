package com.example.springsocial.controller;

import com.example.springsocial.dtos.UniversityDTO;
import com.example.springsocial.dtos.converters.UniversityDTOtoUniversity;
import com.example.springsocial.dtos.converters.UniversityToUniversityDTO;
import com.example.springsocial.model.University;
import com.example.springsocial.services.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/universities")
public class UniversityController {

    @Autowired
    private UniversityService universityService;

    @Autowired
    private UniversityToUniversityDTO universityToUniversityDTO;

    @Autowired
    private UniversityDTOtoUniversity universityDTOtoUniversity;

    @GetMapping
    public ResponseEntity<List<UniversityDTO>> getAll(){
        List<University> universities = universityService.findAll();

        return new ResponseEntity<>(universityToUniversityDTO.convert(universities), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UniversityDTO> getOne(@PathVariable("id") Long id){
        University university = universityService.findById(id);
        if (university == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(universityToUniversityDTO.convert(university), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UniversityDTO> create(@RequestBody UniversityDTO universityDTO){
        University university = universityService.save(universityDTOtoUniversity.convert(universityDTO));

        return new ResponseEntity<>(universityToUniversityDTO.convert(university), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UniversityDTO> update(@RequestBody UniversityDTO universityDTO, @PathVariable("id") Long id){
        if (universityService.findById(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        University university = universityService.save(universityDTOtoUniversity.convert(universityDTO));

        return new ResponseEntity<>(universityToUniversityDTO.convert(university), HttpStatus.OK);
    }

}
