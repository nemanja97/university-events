package com.example.springsocial.controller;

import com.example.springsocial.dtos.FilePathDTO;
import com.example.springsocial.dtos.PasswordDTO;
import com.example.springsocial.dtos.UserDTO;
import com.example.springsocial.dtos.converters.UserDTOtoUser;
import com.example.springsocial.dtos.converters.UserToUserDTO;
import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.model.*;
import com.example.springsocial.payload.AuthResponse;
import com.example.springsocial.payload.LoginRequest;
import com.example.springsocial.repository.UserRepository;
import com.example.springsocial.security.TokenProvider;
import com.example.springsocial.services.CommentService;
import com.example.springsocial.services.LikeDislikeService;
import com.example.springsocial.services.UserGoingToEventService;
import com.example.springsocial.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping("api/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private UserToUserDTO userToUserDTO;

    @Autowired
    private UserDTOtoUser userDTOtoUser;

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private LikeDislikeService likeDislikeService;

    @Autowired
    private UserGoingToEventService userGoingToEventService;

    @Value("${upload.path-users}")
    private String UPLOAD_DIR;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenProvider.createToken(authentication);
        return ResponseEntity.ok(new AuthResponse(token));
    }

    @PostMapping("/signup")
    public ResponseEntity<UserDTO> registerUser(@RequestBody UserDTO userDTO) {
        if(userRepository.existsByEmail(userDTO.getEmail())) {
            throw new BadRequestException("Email address already in use.");
        }
        User user = userDTOtoUser.convert(userDTO);
        user.setRole(UserRole.ROLE_USER);
        user.setProvider(AuthProvider.local);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setDeleted(false);
        user.setEmailVerified(false);
        user.setProviderId("");
        user.setBlocked(false);

        user = userRepository.save(user);

        return new ResponseEntity<UserDTO>(userToUserDTO.convert(user), HttpStatus.CREATED);
    }

    @PostMapping(value = "/upload")
    public  ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) throws IOException, URISyntaxException {
        String filename = file.getOriginalFilename();
        String path = UPLOAD_DIR + File.separator + filename;
        userService.saveFile(file.getBytes(),path);

        return new ResponseEntity(new FilePathDTO(path), HttpStatus.OK);
    }

    @GetMapping(value = "/logged/{email}")
    public  ResponseEntity<UserDTO> getUserByEmail(@PathVariable("email") String email){
        User user = userService.findUserByEmail(email);
        if(user == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(userToUserDTO.convert(user),HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> getAll(Pageable pageable){
        Page<User> users = userService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(users.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<List<UserDTO>>(userToUserDTO.convert(users.getContent()), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDTO> getOne(@PathVariable("id") Long id){
        User user = userService.findById(id);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(userToUserDTO.convert(user), HttpStatus.OK);
    }

    @GetMapping(value = "/search/{text}")
    public ResponseEntity<List<UserDTO>> search(@PathVariable("text") String text, Pageable pageable){
        Page<User> users = userService.search(text, pageable);
        if(users.getSize() == 0){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(users.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<>(userToUserDTO.convert(users.getContent()), headers, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UserDTO> update(@RequestBody UserDTO userDTO, @PathVariable("id") Long id){
        if (userService.findById(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        User user = userDTOtoUser.convert(userDTO);
        User currentUser = userService.findById(id);
        user.setPassword(currentUser.getPassword());
        user.setImageUrl(userService.findById(id).getImageUrl());

        user = userService.save(user);

        return new ResponseEntity<>(userToUserDTO.convert(user), HttpStatus.OK);
    }

    @PutMapping(value = "/change-password")
    public ResponseEntity<?> changePassword(@RequestBody PasswordDTO passwordDTO){

        User user = userService.findById(passwordDTO.getUserDTO().getId());
        user.setPassword(passwordEncoder.encode(passwordDTO.getPassword()));
        userService.save(user);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> remove(@PathVariable("id") Long id){
        User user = userService.findById(id);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        List<UserGoingToEvent> users = userGoingToEventService.findAllByUserID(id);
        for (UserGoingToEvent userG: users){
            userGoingToEventService.remove(userG.getId());
        }
        List<Comment> comments = commentService.findByUserId(id);
        for (Comment comment: comments){
            commentService.remove(comment.getId());
        }
        List<LikeDislike> likes = likeDislikeService.findByUserId(id);
        for (LikeDislike like: likes){
            likeDislikeService.remove(like.getId());
        }

        if (user.getProvider() == AuthProvider.local){
            String url = user.getImageUrl();
            File file = new File(url);
            file.delete();
        }

        userService.remove(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/sort/{first_term}/{second_term}")
    public ResponseEntity<List<UserDTO>> sort(@PathVariable("first_term") String first_term, @PathVariable("second_term") String second_term, Pageable pageable){
        Page<User> users = null;
        if (second_term.equals("ASC")){
            if (first_term.equals("username"))
                users = userService.findAllOrderByUsernameAsc(pageable);
            else if (first_term.equals("firstName"))
                users = userService.findAllOrderByFirstNameAsc(pageable);
            else if (first_term.equals("lastName"))
                users = userService.findAllOrderByLastNameAsc(pageable);
            else if (first_term.equals("email"))
                users = userService.findAllOrderByEmailAsc(pageable);
        }else if (second_term.equals("DESC")){
            if (first_term.equals("username"))
                users = userService.findAllOrderByUsernameDesc(pageable);
            else if (first_term.equals("firstName"))
                users = userService.findAllOrderByFirstNameDesc(pageable);
            else if (first_term.equals("lastName"))
                users = userService.findAllOrderByLastNameDesc(pageable);
            else if (first_term.equals("email"))
                users = userService.findAllOrderByEmailDesc(pageable);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(users.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");
        
        return new ResponseEntity<List<UserDTO>>(userToUserDTO.convert(users.getContent()), headers, HttpStatus.OK);
    }

}
