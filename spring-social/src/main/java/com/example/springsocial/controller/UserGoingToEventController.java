package com.example.springsocial.controller;

import com.example.springsocial.dtos.UserGoingToEventDTO;
import com.example.springsocial.dtos.converters.GoingEventDTOtoGoingEvent;
import com.example.springsocial.dtos.converters.GoingEventToGoingEventDTO;
import com.example.springsocial.model.Event;
import com.example.springsocial.model.User;
import com.example.springsocial.model.UserGoingToEvent;
import com.example.springsocial.services.EventService;
import com.example.springsocial.services.UserGoingToEventService;
import com.example.springsocial.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/confirmEvent")
public class UserGoingToEventController {

    @Autowired
    private UserGoingToEventService userGoingToEventService;

    @Autowired
    private GoingEventToGoingEventDTO goingEventToGoingEventDTO;

    @Autowired
    private GoingEventDTOtoGoingEvent goingEventDTOtoGoingEvent;

    @Autowired
    private EventService eventService;

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<UserGoingToEventDTO>> getAll(){
        List<UserGoingToEvent> users = userGoingToEventService.findAll();

        return new ResponseEntity<>(goingEventToGoingEventDTO.convert(users), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserGoingToEventDTO> getOne(@PathVariable("id") Long id){
        UserGoingToEvent user = userGoingToEventService.findById(id);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(goingEventToGoingEventDTO.convert(user), HttpStatus.OK);
    }

    @GetMapping(value = "/search/{text}")
    public ResponseEntity<List<UserGoingToEventDTO>> search(@PathVariable("text") String text){
        List<UserGoingToEvent> userGoingToEvents = userGoingToEventService.search(text);

        return new ResponseEntity<>(goingEventToGoingEventDTO.convert(userGoingToEvents), HttpStatus.OK);
    }

    @GetMapping(value = "/searchByEvent/{text}")
    public ResponseEntity<List<UserGoingToEventDTO>> searchByEvent(@PathVariable("text") String text){
        List<UserGoingToEvent> userGoingToEvents = userGoingToEventService.searchByEvent(text);

        return new ResponseEntity<>(goingEventToGoingEventDTO.convert(userGoingToEvents), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserGoingToEventDTO> create(@RequestBody UserGoingToEventDTO userDTO){

        UserGoingToEvent userE = goingEventDTOtoGoingEvent.convert(userDTO);

        User user = userService.findById(userDTO.getUserDTO().getId());
        user.setImageUrl(user.getImageUrl());
        user = userService.save(user);

        Long id = userDTO.getEventDTO().getId();
        Event event = eventService.findById(id);
        event.setNumberOfConfirmedArrivals(event.getNumberOfConfirmedArrivals()+1);
        event.setCoverPhoto(event.getCoverPhoto());
        event = eventService.save(event);

        userGoingToEventService.save(userE);
        userE.setEvent(event);
        userE.setUser(user);

        return new ResponseEntity<>(goingEventToGoingEventDTO.convert(userE), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<UserGoingToEventDTO> update(@RequestBody UserGoingToEventDTO userGoingToEventDTO, @PathVariable("id") Long id){
        if (userGoingToEventService.findById(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        UserGoingToEvent userGoingToEvent = userGoingToEventService.save(goingEventDTOtoGoingEvent.convert(userGoingToEventDTO));

        return new ResponseEntity<>(goingEventToGoingEventDTO.convert(userGoingToEvent), HttpStatus.OK);
    }

    @GetMapping(value = "/event/{id}")
    public ResponseEntity<List<UserGoingToEventDTO>> findAllByEventId(@PathVariable("id") Long id){
        List<UserGoingToEvent> userGoingToEvents = userGoingToEventService.findAllByEventId((id));

        return new ResponseEntity<>(goingEventToGoingEventDTO.convert(userGoingToEvents), HttpStatus.OK);
    }

    @GetMapping(value = "/userConfirmed/{eventId}/{userId}")
    public ResponseEntity<UserGoingToEventDTO> findByEventIdAndUserId(@PathVariable("eventId") Long eventId, @PathVariable("userId") Long userId){
        if(eventId == null || userId == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        UserGoingToEvent userGoingToEvent = userGoingToEventService.findOneByEventIdAndUserId(eventId, userId);
        if (userGoingToEvent == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(goingEventToGoingEventDTO.convert(userGoingToEvent), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Integer> delete(@PathVariable("id") Long id){
        if (userGoingToEventService.findById(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        UserGoingToEvent userWhoGo = userGoingToEventService.findById(id);
        Event event = eventService.findById(userWhoGo.getEvent().getId());
        event.setNumberOfConfirmedArrivals(event.getNumberOfConfirmedArrivals()-1);
        event = eventService.save(event);

        userGoingToEventService.delete(id);

        return new ResponseEntity<>(event.getNumberOfConfirmedArrivals(),HttpStatus.OK);
    }

    @GetMapping(value = "/user/{id}")
    public ResponseEntity<List<UserGoingToEventDTO>> findAllByUserId(@PathVariable("id") Long id){
        List<UserGoingToEvent> userGoingToEvents = userGoingToEventService.findAllByUserID(id);

        return new ResponseEntity<>(goingEventToGoingEventDTO.convert(userGoingToEvents), HttpStatus.OK);
    }

}
