package com.example.springsocial.controller;

import com.example.springsocial.dtos.CommentDTO;
import com.example.springsocial.dtos.converters.CommentDTOtoComment;
import com.example.springsocial.dtos.converters.CommentToCommentDTO;
import com.example.springsocial.model.Comment;
import com.example.springsocial.model.Event;
import com.example.springsocial.model.User;
import com.example.springsocial.services.CommentService;
import com.example.springsocial.services.EventService;
import com.example.springsocial.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/comments")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentToCommentDTO commentToCommentDTO;

    @Autowired
    private CommentDTOtoComment commentDTOtoComment;

    @Autowired
    private EventService eventService;

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<CommentDTO>> getAll(Pageable pageable){
        Page<Comment> comments = commentService.findAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(comments.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<List<CommentDTO>>(commentToCommentDTO.convert(comments.getContent()), headers, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CommentDTO> getOne(@PathVariable("id") Long id){
        Comment comment = commentService.findById(id);
        if (comment == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(commentToCommentDTO.convert(comment), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<CommentDTO> create(@RequestBody CommentDTO commentDTO){
        Comment comment = commentService.save(commentDTOtoComment.convert(commentDTO));

        User user = userService.findById(comment.getUser().getId());
        user.setImageUrl(user.getImageUrl());
        user = userService.save(user);

        Event event = eventService.findById(comment.getEvent().getId());
        event.setCoverPhoto(event.getCoverPhoto());
        event = eventService.save(event);

        comment.setEvent(event);
        comment.setUser(user);

        return new ResponseEntity<>(commentToCommentDTO.convert(comment), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<CommentDTO> update(@RequestBody CommentDTO commentDTO, @PathVariable("id") Long id){
        if (commentService.findById(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Comment comment = commentService.save(commentDTOtoComment.convert(commentDTO));

        return new ResponseEntity<>(commentToCommentDTO.convert(comment), HttpStatus.OK);
    }

    @GetMapping(value = "/event/{id}")
    public ResponseEntity<List<CommentDTO>> getAllByEventId(@PathVariable("id") Long id){
        List<Comment> comments = commentService.getByEventId(id);
        List<Comment> comm = new ArrayList<>();
        for (Comment c: comments){
            if (c.getUser().isBlocked() == false){
                comm.add(c);
            }
        }

        return new ResponseEntity<>(commentToCommentDTO.convert(comm), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> remove(@PathVariable("id") Long id){
        Comment comment = commentService.findById(id);
        if (comment == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        commentService.remove(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
