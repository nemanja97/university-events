package com.example.springsocial.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileWriter;
import java.io.IOException;

@RestController
@RequestMapping("api/live-stream")
public class LiveStreamController {

    static String putanja = "C:\\Users\\natasa_gacic\\Desktop\\university-events\\spring-social\\src\\main\\resources\\";

    @GetMapping(value = "/start")
    public String startLiveStream() {
        return start();
    }

    @GetMapping(value = "/stop")
    public String stopLiveStream() {
        return stop();
    }


    public static String start() {
        String[] list = {"python", putanja + "live.py"};
        ProcessBuilder processBuilder = new ProcessBuilder(list);
        processBuilder.command(list);
        try {
            Process process = processBuilder.start();
        } catch (IOException e) {
            e.printStackTrace();
            return "Greska: " + e.toString();
        }
        return "Uspesno";
    }


    public static String stop() {
        try {
            FileWriter myWriter = new FileWriter(putanja + "c.txt");
            myWriter.write("f");
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            return "Greska: " + e.toString();
        }
        return "Uspesno";
    }

}
