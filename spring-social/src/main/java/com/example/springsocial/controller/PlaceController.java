package com.example.springsocial.controller;

import com.example.springsocial.dtos.PlaceDTO;
import com.example.springsocial.dtos.converters.PlaceDTOtoPlace;
import com.example.springsocial.dtos.converters.PlaceToPlaceDTO;
import com.example.springsocial.model.Place;
import com.example.springsocial.services.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "api/places")
public class PlaceController {

    @Autowired
    private PlaceService placeService;

    @Autowired
    private PlaceToPlaceDTO placeToPlaceDTO;

    @Autowired
    private PlaceDTOtoPlace placeDTOtoPlace;

    @GetMapping
    public ResponseEntity<List<PlaceDTO>> findAll(){
        List<Place> places = placeService.findAll();

        return new ResponseEntity<>(placeToPlaceDTO.convert(places), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PlaceDTO> getOne(@PathVariable("id") Long id){
        Place place = placeService.findById(id);
        if (place == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(placeToPlaceDTO.convert(place), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PlaceDTO> create(@RequestBody PlaceDTO placeDTO){
        Place place = placeService.save(placeDTOtoPlace.convert(placeDTO));

        return new ResponseEntity<>(placeToPlaceDTO.convert(place), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<PlaceDTO> update(@RequestBody PlaceDTO placeDTO, @PathVariable("id") Long id){
        if (placeService.findById(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Place place = placeService.save(placeDTOtoPlace.convert(placeDTO));

        return new ResponseEntity<>(placeToPlaceDTO.convert(place), HttpStatus.OK);
    }

}
