package com.example.springsocial.controller;

import com.example.springsocial.dtos.EventDTO;
import com.example.springsocial.dtos.FilePathDTO;
import com.example.springsocial.dtos.converters.EventDTOtoEvent;
import com.example.springsocial.dtos.converters.EventToEventDTO;
import com.example.springsocial.model.Comment;
import com.example.springsocial.model.Event;
import com.example.springsocial.model.LikeDislike;
import com.example.springsocial.model.UserGoingToEvent;
import com.example.springsocial.services.CommentService;
import com.example.springsocial.services.EventService;
import com.example.springsocial.services.LikeDislikeService;
import com.example.springsocial.services.UserGoingToEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;

import java.util.List;
import java.net.URISyntaxException;
import java.io.File;

@RestController
@RequestMapping(value = "api/events")
public class EventController {

    @Autowired
    private EventService eventService;

    @Autowired
    private EventToEventDTO eventToEventDTO;

    @Autowired
    private EventDTOtoEvent eventDTOtoEvent;

    @Autowired
    private UserGoingToEventService userGoingToEventService;

    @Autowired
    private LikeDislikeService likeDislikeService;

    @Autowired
    private CommentService commentService;

    @Value("${upload.path}")
    private String UPLOAD_DIR;

    @GetMapping(value= "/live")
    public ResponseEntity<List<EventDTO>> getLive(){
        List<Event> events = eventService.liveEvents();

        return new ResponseEntity<List<EventDTO>>(eventToEventDTO.convert(events), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<EventDTO>> getAll(Pageable page){
        Page<Event> events = eventService.findAll(page);
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(events.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<List<EventDTO>>(eventToEventDTO.convert(events.getContent()),headers, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<EventDTO> getOne(@PathVariable("id") Long id){
        Event event = eventService.findById(id);
        if (event == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(eventToEventDTO.convert(event), HttpStatus.OK);
    }

    @GetMapping(value = "/search/{text}")
    public ResponseEntity<List<EventDTO>> search(@PathVariable("text") String text, Pageable pageable){
        Page<Event> events = eventService.search(text, pageable);
        if(events.getSize() == 0)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(events.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<>(eventToEventDTO.convert(events.getContent()), headers, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<EventDTO> create(@RequestBody EventDTO eventDTO){
        Event event = eventService.save(eventDTOtoEvent.convert(eventDTO));

        return new ResponseEntity<>(eventToEventDTO.convert(event), HttpStatus.CREATED);
    }

    @PostMapping(value = "/upload")
    public  ResponseEntity<String> uploadFile(@RequestParam("file")MultipartFile file) throws IOException, URISyntaxException {
        String filename = file.getOriginalFilename();
        String path = UPLOAD_DIR + File.separator + filename;
        eventService.saveFile(file.getBytes(),path);

        return new ResponseEntity(new FilePathDTO(path), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<EventDTO> update(@RequestBody EventDTO eventDTO, @PathVariable("id") Long id){
        if (eventService.findById(id) == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Event event = eventDTOtoEvent.convert(eventDTO);
        event.setCoverPhoto(eventService.findById(id).getCoverPhoto());

        eventService.save(event);

        return new ResponseEntity<>(eventToEventDTO.convert(event), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> remove(@PathVariable("id") Long id){
        Event event = eventService.findById(id);
        if (event == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        List<UserGoingToEvent> users = userGoingToEventService.findAllByEventId(id);
        for (UserGoingToEvent user: users){
            userGoingToEventService.remove(user.getId());
        }
        List<Comment> comments = commentService.getByEventId(id);
        for (Comment comment: comments){
            commentService.remove(comment.getId());
        }
        List<LikeDislike> likes = likeDislikeService.findByEventId(id);
        for (LikeDislike like: likes){
            likeDislikeService.remove(like.getId());
        }

        String url = event.getCoverPhoto();
        File file = new File(url);
        file.delete();

        eventService.remove(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/sort/{first_term}/{second_term}")
    public ResponseEntity<List<EventDTO>> sort(@PathVariable("first_term") String first_term, @PathVariable("second_term") String second_term, Pageable pageable){
        Page<Event> events = null;
        if (second_term.equals("ASC")){
            if (first_term.equals("title"))
                events = eventService.findAllOrderByTitleAsc(pageable);
            else if (first_term.equals("date"))
                events = eventService.findAllOrderByDateAsc(pageable);
            else if (first_term.equals("number_of_confirmed_arrivals"))
                events = eventService.findAllOrderByConfirmedArrivalsAsc(pageable);
        }else if (second_term.equals("DESC")){
            if (first_term.equals("title"))
                events = eventService.findAllOrderByTitleDesc(pageable);
            else if (first_term.equals("date"))
                events = eventService.findAllOrderByDateDesc(pageable);
            else if (first_term.equals("number_of_confirmed_arrivals"))
                events = eventService.findAllOrderByConfirmedArrivalsDesc(pageable);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("totalPages",Integer.toString(events.getTotalPages()));
        headers.add("access-control-expose-headers","totalPages");

        return new ResponseEntity<List<EventDTO>>(eventToEventDTO.convert(events.getContent()), headers, HttpStatus.OK);
    }

}
